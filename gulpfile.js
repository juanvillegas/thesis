var browserify = require('browserify');
var babel = require('gulp-babel');
var gulp = require("gulp");
var del = require('del');


gulp.task("dist", function() {

    // set up the browserify instance on a task basis
    var b = browserify({
        entries: "build/js/bundler.js",
        debug: true,
        // defining transforms here will avoid crashing your stream
        // transform: [reactify]
    });

    return b.bundle()
        // .pipe(buffer())
        // .pipe(sourcemaps.init({loadMaps: true}))
        // Add transformation tasks to the pipeline here.
        // .pipe(uglify())
        // .on('error', gutil.log)
        // .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest("dist/js/"));
});


gulp.task("commonjs", function() {
	return del("build-common/**").then(() => {
		return gulp.src("src/js/**/*.js")
			.pipe(babel({ plugins: ["transform-es2015-modules-commonjs"] }))
			.pipe(gulp.dest("build-common/js"));
	});

});

gulp.task("build", function() {
    return gulp.src("src/js/**/*.js")               // #1. select all js files in the app folder
        .pipe(babel({ presets: ['es2015'] }))    // #2. transpile ES2015 to ES5 using ES2015 preset
        .pipe(gulp.dest("build/js"));               // #3. copy the results to the build folder
});