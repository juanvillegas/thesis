# Tesis-Core

**Tesis-Core** es la implementación del Framework desarrollado en el proyecto de Tesis.
Los clientes pueden conectarse de forma remota para simular conversaciones. Cada conexión
genera una simulación nueva.

**Autor**: Juan Manuel Villegas

## Build & Execution

``` bash
# instalar dependencias
npm install

# compilar commonjs
npm run build

# ejecutar socket
npm run run
```

*npm run run* inicializa un Socket en espera de conexiones en un puerto definido en .env. Para
conectarse, ejecutar alguno de los clientes provistos (ver https://gitlab.com/juanvillegas/thesis-shell).

>Creado por **Juan Manuel Villegas** @ 2017