var axios = require('axios');
require('dotenv').config();

let params = {
    headers: {
        "Authorization": "Token " + process.env.RECAST_API_KEY,
        "Content-Type": "application/json"
    },
    url: process.env.RECAST_API_REMOTE,
    method: "POST",
    data: JSON.stringify({ language: "en", text: "Book me a flight" }),
    responseType: "json"
};

return axios(params).then(response => {
    console.log('Recast ----> [[[HEALTHY]]]');
}, error => {
    console.log(error);
    console.log('Recast ----> [[[DOWN]]]');
});

