var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
require('dotenv').config();
var ChatbotFactory = require('../build-common/js/chatterbot/chatterbot-factory').default;

io.on('connection', function(socket){
	console.log('[ChatbotServer] Socket connected');

	socket.on('disconnect', () => {
		console.log('[ChatbotServer] Socket disconnected');
		socket.disconnect(true);
	});

	socket.on('chatterbot.handshake', function(config) {
		config.socket = socket;
		var botInstance = ChatbotFactory.makeInstance(config);

		botInstance.execute({
			debug: true
		});

		socket.emit('chatterbot.inited');
	});
});

var port = process.env.PORT  ? process.env.PORT  : 7777;

http.listen(port, function(){
	console.log('[ChatbotServer] Listening in port: ' + port);
});