export default class BotLog {


    static log(message) {
        console.log("[Chatterbot] " + message);
    }

    static logAction(message) {
        BotLog.log(message);
    }

    static logWarning(message) {
        console.warn("[Chatterbot] " + message);
    }

    static logError(message) {
        console.error("[Chatterbot] " + message);
    }

    static debug(message) {
        console.debug("[BOT DEBUG] =============");
        console.debug("[BOT DEBUG] " + message);
        console.debug("[BOT DEBUG] =============");
    }

    static speech(utterance) {
        console.log(`[Bot] says: '${utterance}'`);
    }
}