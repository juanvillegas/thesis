import FlyReservationChatterbot from '../scenarios/flight-reservation/flight-reservation-bot';
import SocketCom from './components/socket-com';


export default class ChatterbotFactory {

	static makeInstance(config) {
		if (! config.scenario) {
			throw "Configuration error when trying to instantiate a new Chatbot";
		}

		switch (config.scenario) {
			case "flybot":
				return new FlyReservationChatterbot(new SocketCom(config.socket), config);
		}

		throw "The required scenario ID '" + config.scenario + "' is invalid.";
	}

}