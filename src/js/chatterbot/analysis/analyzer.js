import UtteranceDescriptor from "./utterance-descriptor";
import _ from "lodash";
import BotLog from "../utils/bot-log";
import axios from "axios";
import Entity from "../model/Entity";
import Intent from "../model/Intent";

export default class Analyzer {

    constructor(chatterbot, rsBrains)
    {
        this._chatterbot = chatterbot;
        this.rsBrains = rsBrains;
        this.reset();
    }

    reset() {
        this._utterance = this._chatterbot.getMemory("last_user_utterance");
        this._lastSystemUtterance = this._chatterbot.getMemory("last_system_utterance");
        this.currentGoal = this._chatterbot.currentGoal;

        this._analysis = {
            binary: false,
            options: false,
            nlu: false,
            rive: false,
            direct: false
        };
    }

    /**
     * Analyzed the given utterance, and returns an UtteranceDetails object as a result.
     * The type of analysis depends on subclass implementations, but could be NLU, AIML, manual pattern matching, etc.
     */
    analyze()
    {
        let analysis = this._extractMeaning();

        return Promise.all(analysis).then(results => {
            let resultsDict = {};

            results.forEach(aResult => {
                if (aResult) {
                    resultsDict[aResult.type] = aResult.descriptor;
                }
            });

			if (! resultsDict.rive) {
				return this._nlu(this._utterance.utterance)
					.then(jsonResponse => {
						if (jsonResponse) {
							resultsDict["nlu"] = UtteranceDescriptor.createFromNLU(jsonResponse.results);

							if (this._lastSystemUtterance.type == "options-query") {
								// TODO: interpretOptionsQuery should receive an array of resultsDict items instead, so as to include
								// rive and nlu results, not just nlu.
								this._interpretOptionsQuery(resultsDict.nlu);
							}
						}

						return resultsDict;
					});
			}

			return resultsDict;
        }).then(results => {
            if (results.binary && this._chatterbot.MEMORY.hasContextOfType("select-options")) {
                let context = this._chatterbot.MEMORY.getContext("select-flight-context");

                // the user may have selected the "current" option by speaking something like: "Yes", "Yea", etc..
                // basically a positive binary query under an options context will end here...
                let selection = results.binary.getEntity("option");
                if (selection.value == "current-option") {
                    let ud = new UtteranceDescriptor();
                    let entity = new Entity("selection", context.getActiveOption(), context.getActiveOption());
                    ud.addEntity(entity);
                    ud.addIntent(new Intent("select-option"));
                    results.options = ud;
                }
            }

            return this._interpret(results);
        });
    }

    /**
     * Select which of the results we want to use, depending on some heuristics
     * @param results
     * @private
     */
    _interpret(results) {
		if (results.generics) {
			return results.generics;
		}

        if (results.direct) {
            return results.direct;
        } else {
            if (results.binary) {
                if (results.options) {
                    return results.options;
                } else {
                    return results.binary;
                }
            } else {
                if (results.options) {
                    return results.options;
                } else {
                    if (results.rive) {
                        return results.rive;
                    } else {
                    	if (results.nlu) {
                        	return results.nlu;
						} else {
                    		// extreme case, nothing could be obtained from input... :(
                    		return {};
						}
                    }
                }
            }
        }
    }

    /**
     * to be overridden
     */
    _extractMeaning() {
        let analysisList = [];

		analysisList.push(this._interpretGenerics());

        if (this._lastSystemUtterance.type == "binary-query") {
            analysisList.push(this._interpretBinaryQuery());
        }

        if (this._lastSystemUtterance.type == "direct-query") {
            analysisList.push(this._interpretDirectQuery());
        }

        if (this._chatterbot.MEMORY.hasContextOfType("select-options")) {
            analysisList.push(this._analyzeSelectResultsContext());
        }

        if (this.currentGoal.brain) {
            analysisList.push(this._interpretGoalBrain(this.currentGoal.brain));
        }

        return analysisList;
    }

    _interpretGenerics() {
		return this._rive(this.rsBrains['generics'].instance, this._utterance.utterance)
			.then(jsonResponse => {
				BotLog.log("[NLU-Rive] [Module=generics] Answered:");
				BotLog.log("[NLU-Rive] [Module=generics] " + JSON.stringify(jsonResponse));
				if (jsonResponse) {
					return { type: "generics", descriptor: UtteranceDescriptor.createFromJSON(jsonResponse) };
				} else {
					BotLog.log("[NLU-Rive] [Module=generics] Answered: false/couldn't parse to Json, check your .rive s");
					return false;
				}
			}, () => {
				BotLog.log("[NLU-Rive] [Module=generics] Answered: false/error");
				return false;
			});
	}

    _interpretGoalBrain(brainName) {
        return this._rive(this.rsBrains[brainName].instance, this._utterance.utterance)
                .then(jsonResponse => {
                    BotLog.log("[NLU-Rive] Answered:");
                    BotLog.log("[NLU-Rive] " + JSON.stringify(jsonResponse));
                    if (jsonResponse) {
                        return { type: "rive", descriptor: UtteranceDescriptor.createFromJSON(jsonResponse) };
                    } else {
                    	BotLog.log("[NLU-Rive] Answered: false/couldn't parse to Json, check your .rive s");
                        return false;
                    }
                }, () => {
                    BotLog.log("[NLU-Rive] Answered: false/error");
                    return false;
                });
    }

    _interpretDirectQuery() {
        if (this._lastSystemUtterance.brain) {
            if (this.rsBrains[this._lastSystemUtterance.brain]) {
                return this._rive(this.rsBrains[this._lastSystemUtterance.brain].instance, this._utterance.utterance)
                    .then(jsonResponse => {
                        if (jsonResponse.success) {
                        	let descriptor = { type: "direct", descriptor: UtteranceDescriptor.createFromJSON(jsonResponse) };
							BotLog.log("[DirectQuery] " + JSON.stringify(descriptor));
                            return descriptor;
                        }
                    });
            }
        }

        return false;
    }

    _interpretOptionsQuery(utterance) {
        if (utterance.hasAnyIntentOf(["options-query-negate", "no"])) {
            let updateVals = _.get(this._lastSystemUtterance, "if-true");
            updateVals = updateVals.split(",");

            utterance.addIntent(new Intent(updateVals[0]));
            let anEntity = new Entity(updateVals[1], updateVals[2]);
            utterance.addEntity(anEntity);

            utterance.removeIntentByName("options-query-negate");

			BotLog.log("[OptionsQuery] " + JSON.stringify(utterance));
        }
    }

    _interpretBinaryQuery() {
        return this._rive(this.rsBrains.directQueryMatcher.instance, this._utterance.utterance)
            .then(jsonResponse => {
                if (jsonResponse.success) {
                    let utteranceDescriptor = new UtteranceDescriptor();

                    if (jsonResponse.type == "positive") {
                        // the query was accepted by the user, so a proper utterance needs to be built to reflect this
                        let trueValue = _.get(this._lastSystemUtterance, "true-value", false);
                        if (trueValue) {
                            let anEntity = new Entity(this._lastSystemUtterance.subject, trueValue);
                            utteranceDescriptor.addEntity(anEntity);
                            utteranceDescriptor.addIntent(new Intent("any"));
                        }

                        let trueIntent = _.get(this._lastSystemUtterance, "true-intent", false);
                        if (trueIntent) {
                            utteranceDescriptor.addIntent(new Intent(trueIntent));
                        }
                    } else {
                        // the query was rejected by the user (no, nope, neh, nah, etc)...
                        let falseValue = _.get(this._lastSystemUtterance, "false-value", false);
                        if (falseValue) {
                            let anEntity = new Entity(this._lastSystemUtterance.subject, falseValue);
                            utteranceDescriptor.addEntity(anEntity);
                            utteranceDescriptor.addIntent(new Intent("any"));
                        }

                        let falsyIntent = _.get(this._lastSystemUtterance, "false-intent", false);
                        if (falsyIntent) {
                            utteranceDescriptor.addIntent(new Intent(falsyIntent));
                        }
                    }

                    let descriptor = { type: "binary", descriptor: utteranceDescriptor };
					BotLog.log("[BinaryQuery] " + JSON.stringify(descriptor));
                    return descriptor;
                }

                return false;
            });
    }

    _analyzeSelectResultsContext() {
        return this._rive(this.rsBrains.selectOptions.instance, this._utterance.utterance)
            // if RiveScript was able to match the utterance to a valid option selector, then the promise will accept(), else
            // the "falsy" value will be propagated indicating there was no match
            .then(response => UtteranceDescriptor.createFromJSON(response), () => false)
            .then(anUtteranceDescriptor => {
                // a valid UD indicates the user has selected an option: "I will choose the third option"
                if (anUtteranceDescriptor !== false) {
                    if (anUtteranceDescriptor.hasIntent("select-option")) {
                        // the user spoke something like, "actually, can I get the 2nd option?"
                        // the processor updates the current "select-options" context with the matching value, if it exists
                        let context = this._chatterbot.MEMORY.getContext("select-flight-context"); // TODO: hard coded context name...
                        let selectionIndex = anUtteranceDescriptor.getEntity("ordinal");

                        if (selectionIndex !== false) {
                            let value = selectionIndex.value;
                            // TODO: ordinal to number here
                            let selection = context.getOption(0);

                            if (selection !== false) {
                                let entity = new Entity("selection", selection, selection);
                                anUtteranceDescriptor.addEntity(entity);
                            }
                        } else {
                            // the user may have selected the "current" option
                            let selection = anUtteranceDescriptor.getEntity("option");
                            if (selection.value == "current-option") {
                                let entity = new Entity("selection", context.getActiveOption(), context.getActiveOption());
                                anUtteranceDescriptor.addEntity(entity);
                            }
                        }
                    }

                    let descriptor = { type: "options", descriptor: anUtteranceDescriptor };
					BotLog.log("[ContextQuery] " + JSON.stringify(descriptor));
                    return descriptor;
                } else {
                    return false;
                }
            });
    }

    _rive(riveBrain, utteranceStr) {
        let theReply = riveBrain.reply("juan", utteranceStr);

        return new Promise((accept, reject) => {
            if (theReply == "ERR: No Reply Matched") {
                reject();
            }
            accept(JSON.parse(theReply));
        });
    }

    _nlu(utterance)
    {
    	let apiKey = process.env.RECAST_API_KEY;
    	let apiRemote = process.env.RECAST_API_REMOTE;

        let params = {
            headers: {
                "Authorization": "Token " + apiKey,
                "Content-Type": "application/json"
            },
			url: apiRemote,
            method: "POST",
            data: JSON.stringify({ language: "en", text: utterance }),
			responseType: "json"
        };

        return axios(params).then(response => {
            BotLog.log("[NLU] Response:");
            BotLog.log(JSON.stringify(response.data));
            return response.data;
        }, error => {
            BotLog.log("[NLU] Errored:");
            BotLog.log(error.error);
            return false;
		});
    }

}