import _ from "lodash";
import Entity from "../model/Entity";
import LocationEntity from "../model/LocationEntity";
import DateEntity from "../model/DateEntity";
import DateIntervalEntity from "../model/DateIntervalEntity";
import Intent from "../model/Intent";

export default class UtteranceDescriptor {

    constructor() {
        this._entities = [];
        this._intents = [];
        this._utterance = "";
        this._speechAct = "";
        /**
         * stuff like sentiment, language, acts, timestamps, or whatever the api returns that we may find
         * useful to include here.
         * @type {Object}
         * @private
         */
        this._extra = {
            sentiment: false, // enum: one of [positive, negative, neutral]
        };
    }

    /**
     * @returns {string}
     */
    get utterance() {
        return this._utterance;
    }

    getMainIntent() {
        return this._intents.length >= 1 ? this._intents[0] : false;
    }

    hasIntent(intentStr) {
        return this.hasAnyIntentOf([intentStr]);
    }

    hasAnyIntentOf(intentArray) {
        return this._intents.filter(intent => {
            return intentArray.indexOf(intent.name) >= 0;
        }).length > 0;
    }

    removeIntentByName(intentStr) {
        this._intents = this._intents.filter(intent => {
            return intent.name != intentStr;
        });
    }

    get speechAct() {
        return this._speechAct;
    }

    get sentiment() {
        return this._extra.sentiment;
    }

    addEntity(entity) {
        this._entities.push(entity);
    }

    addIntent(intent) {
        this._intents.push(intent);
    }

    /**
     * @returns {Array} of Entity objects
     */
    getEntities() {
        return this._entities;
    }

    /**
     * Gets an entity by its name, if it exists in the utterance descriptor
     * @param name
     * @returns {*}
     */
    getEntity(name) {
        let matching = this.getEntities().filter(e => e.name == name);
        if (matching.length > 0) {
            return matching[0];
        }
        return false;
    }

    /**
     * Helper method. Creates an utterance descriptor using the result from
     * the NLU parser as input
     *
     * @param {Object} input As received from the NLU engine (atm, Recast.AI). Ie: includes .source, .intents, .entities, etc
     *
     */
    static createFromNLU(input) {
        let descriptor = new UtteranceDescriptor();

        descriptor._speechAct = _.get(input, "act", false);
        descriptor._extra.sentiment = _.get(input, "sentiment", "neutral");

        descriptor._entities = _.filter(_.map(_.get(input, "entities", []), (entityEntries, entityName) => {
            // choose the most confident one from each "category" (where category is the entityName)
            let mostConfident = _.last(_.sortBy(entityEntries, "confidence"));
            let entityValue,
                retVal;

            if (entityName == "datetime") {
                entityValue = mostConfident.iso;
                retVal = new DateEntity(entityName, entityValue, {
                    raw: mostConfident.raw,
                    accuracy: mostConfident.accuracy ? mostConfident.accuracy.split(",") : [],
                    isFuture: mostConfident.chronology == "future",
                    source: mostConfident
                });
            } else if (entityName == "interval") {
                // TODO: ???
                retVal = new DateIntervalEntity(entityName, {
                    begin: mostConfident.begin,
                    end: mostConfident.end,
                    source: mostConfident
                })
            } else if ("location" == entityName) {
                // DOC
                // If the type of the entity is "Location" it will contain additional information, because its a native Recast type
                // If the type is not "Location", then it will work just as a regular entity, because its a custom entity created by
                // the bot developer
                let entityValue = mostConfident.raw || mostConfident.formatted;
                retVal = UtteranceDescriptor.parseLocation(input.source, entityName, entityValue);
            } else if (["destination_location", "destination-location"].indexOf(entityName) >= 0) {
                retVal = UtteranceDescriptor.parseLocation(input.source, entityName, mostConfident.value);
            } else if (["departure_location", "departure-location"].indexOf(entityName) >= 0) {
                retVal = UtteranceDescriptor.parseLocation(input.source, entityName, mostConfident.value);
            } else {
                entityValue = mostConfident.value;
                retVal = new Entity(entityName, entityValue, {
                    raw: mostConfident.raw || false,
                    source: mostConfident
                });
            }

            return retVal;
        }));

        descriptor._intents = _.map(_.get(input, "intents", []), (intentObject) => {
            return Intent.createFromNLU(intentObject);
        });

        return descriptor;
    }

    static parseLocation(utterance, entityName, locationValue)
    {
        let needle = locationValue.toLowerCase();
        let matches = utterance.match(new RegExp("(\\w+)\\s" + needle, 'i'));

        entityName = "location";
        if (matches && matches.length) {
            if (matches[1] == "from") {
                entityName = "departure-location";
            } else if (matches[1] == "to") {
                entityName = "destination_location";
            }
        }

        return new LocationEntity(entityName, locationValue);
    }

    static createFromJSON(input) {
        let descriptor = new UtteranceDescriptor();

        descriptor._entities = _.map(_.get(input, "entities", []), entity => {
            let entityValue,
                retVal;

            /*if (entity.name == "datetime") {
                entityValue = entity.iso;
                retVal = new DateEntity(entity.name, entityValue, {
                    raw: entity.raw,
                    accuracy: entity.accuracy ? entity.accuracy.split(",") : [],
                    isFuture: entity.chronology == "future",
                    source: entity
                });
            } else if (entity.name == "interval") {
                // TODO:
                new DateIntervalEntity(entity.name, {
                    begin: entity.begin,
                    end: entity.end,
                    source: entity
                })
            } else*/ if (["location", "destination_location", "departure-location"].indexOf(entity.name) >= 0) {
                retVal = new LocationEntity(entity.name, _.startCase(entity.value.toLowerCase()), {
                    source: entity
                });
            } else {
                entityValue = entity.value;
                retVal = new Entity(entity.name, entityValue, {
                    raw: _.get(entity, "raw", false),
                    source: entity
                });
            }

            return retVal;
        });
        descriptor._intents = _.map(_.get(input, "intents", []), (intentObject) => {
            return Intent.createFromJSON(intentObject);
        });

        return descriptor;
    }

    static createFromString(input) {
        let descriptor = new UtteranceDescriptor();

        descriptor._utterance = input;

        return descriptor;
    }

    static merge(u1, u2) {
        u1._entities = u2._entities;
        u1._intents = u2._intents;

        return u1;
    }

}