import { LocationSlot, DateSlot, BooleanSlot, Slot } from "./Slots";

export default class SlotFactory {

    static create(type, data) {
        switch (type) {
            case "boolean":
                return new BooleanSlot(data.value, data);
            case "date-interval":
                return new DateSlot(data.value, data);
            case "location":
                return new LocationSlot(data.value, data);
            default:
                return new Slot(data.value, data);
        }
    }

}