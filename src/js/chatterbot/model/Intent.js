export default class Intent {

    constructor(name) {
        this._name = name;

        this._confidence = false;
    }

    static createFromNLU(data) {
        let instance = new Intent(data.slug);
        instance._confidence = data.confidence;
        return instance;
    }

    static createFromJSON(data) {
        return new Intent(data);
    }

    get name() {
        return this._name;
    }
}