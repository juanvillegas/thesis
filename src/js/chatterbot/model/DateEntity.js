import Entity from "./Entity";
import moment from "moment";

export default class DateEntity extends Entity {

    constructor(name, value, data = []) {
        super(name, value, data);

        this._accuracy = data.accuracy || [];
        this._isFuture = data.isFuture || false;
    }

    hasDateAccuracy() {
        if (this._accuracy.indexOf("day") >= 0) {
            return true;
        }
    }

    hasTimeAccuracy() {

    }

    get accuracy() {
        return this._accuracy;
    }

    get isFuture() {
        return this._isFuture;
    }

    get accuracyString() {
        return this._accuracy.join(",");
    }
}