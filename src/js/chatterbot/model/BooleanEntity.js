import Entity from "./Entity";

export default class BooleanEntity extends Entity {

    constructor(name, value) {
        super(name, value, []);
    }

    static parse(value) {
        let bool = value == "true";

        return new BooleanEntity("_", bool);
    }

}