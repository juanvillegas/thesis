import Entity from "./Entity";

export default class DateIntervalEntity extends Entity {

    constructor(name, value, data = []) {
        super(name, value, data);

        this._begin = data.begin || false;
        this._end = data.end || false;
    }

    get begin() {
        return this._begin;
    }

    get end() {
        return this._end;
    }

}