export default class Entity {

    constructor(name, value, data = []) {
        this._name = name;
        this._value = value;
        this._raw = data.raw || false;
        this._source = data.source || {};
    }

    get name() {
        return this._name;
    }

    get value() {
        return this._value;
    }

    get raw() {
        return this._raw;
    }

    get source() {
        return this._source;
    }
}