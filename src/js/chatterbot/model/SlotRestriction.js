var moment = require("moment");

export default class SlotRestriction {

    constructor() {
        this._goal = '';
        this._slot = '';
        this._value = '';
        this._type = '';
        this._nice = '';
        this._rules = [];
    }

    get nice() {
        return this._nice;
    }

    get goal() {
        return this._goal;
    }

    get slot() {
        return this._slot;
    }

    get rules() {
        return this._rules;
    }

    static createFromDefinition(definition) {
        let instance = new SlotRestriction();

        instance._goal = definition.goal;
        instance._slot = definition.slot;
        instance._value = definition.value;
        instance._type = definition.type;
        instance._rules = definition.rules || [];
        instance._nice = definition.nice || "";

        return instance;
    }

    validate(slot) {
        let validationErrors = [];

        if (!slot.isCompleted()) {
            return validationErrors;
        }

        let testResult = true;
        switch (this._type) {
            case "date-interval":
                let fromDate, toDate;
                if (this._value == "tomorrow") {
                    fromDate = moment.utc().add(1, 'day').startOf('day');
                    toDate = moment.utc().add(1, 'day').endOf('day').add(6, 'hour').add(1, 'minute');
                } else {
                    fromDate = moment.utc(this._value.from);
                    toDate = moment.utc(this._value.to);
                }

                let slotFromDate = slot.from;
                let slotToDate = slot.to;

                if (slotFromDate.isBefore(fromDate) || slotToDate.isAfter(toDate)) {
                    validationErrors.push('Date interval is not inside the accepted boundaries');
                }

                if (this.rules.length > 0) {
                    this.rules.forEach(r => {
                        let ruleParts = r.split(":");
                        switch (ruleParts[0]) {
                            case "restrictions":
                                switch (ruleParts[1]) {
                                    case "night":
                                        fromDate.hour(21);
                                        toDate.add(1, 'day').hour(6);
                                        if (slotFromDate.isBefore(fromDate, 'hour') || slotToDate.isAfter(toDate, 'hour')) {
                                            validationErrors.push('Date is expected to be at night');
                                        }
                                }
                                break;
                        }
                    });
                }
                return validationErrors;
            case "number-interval":
                let min = this._value[0];
                let max = this._value[1];
                if (!(min <= slot.value && slot.value <= max)) {
                    validationErrors.push(`Provided ${slot.value} is not between ${min} and ${max}`);
                }
                return validationErrors;
            case "boolean-equality":
                testResult = !!this._value == !!slot.value;
                if (!testResult) {
                    validationErrors.push(slot.value + " is expected to be " + this._value);
                }
                return validationErrors;
            case "strict-equality":
                testResult = this._value === slot.value;
                if (!testResult) {
                    validationErrors.push(slot.value + " is expected to be " + this._value);
                }
                return validationErrors;
            case "equality":
                testResult = this._value == slot.value;
                if (! testResult) {
                    validationErrors.push(slot.value + " is expected to be " + this._value);
                }
                return validationErrors;
            case "loose-equality":
                testResult = this._value.toLowerCase() == slot.value.toLowerCase();
                if (! testResult) {
                    validationErrors.push(slot.value + " is expected to be " + this._value);
                }
                return validationErrors;
            case "alpha-equality":
                let regex = new RegExp('[^a-zA-Z0-9]');
                testResult = this._value.toLowerCase() == slot.value.replace(regex, '').toLowerCase();
                if (! testResult) {
                    validationErrors.push(slot.value + " is expected to be " + this._value);
                }
                return validationErrors;
            case "inset":
                testResult = this._value.indexOf(slot.value) >= 0;
                if (! testResult) {
                    validationErrors.push(slot.value + " is expected to be one of " + this._value.join(', '));
                }
                return validationErrors;
        }

        return false;
    }

    bakeErrors(slot) {
        //TODO: this function should return a list of readable errors for whatever restriction not met by the given slot.
    }

}