export default class Utterance {

    constructor(value, topics = [], type = "sentence", language = false) {
        this._value = value;
        this._topics = topics;
        this._type = type;
        this._language = language;
    }

    /**
     *
     * @param {object} utteranceObject
     */
    static createFromObject(utteranceObject) {
        return new Utterance(utteranceObject.value, utteranceObject.topics, utteranceObject.type, utteranceObject.language);
    }

}