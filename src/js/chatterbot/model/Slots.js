import moment from "moment";
import _ from "lodash";
import SlotFactory from "./SlotFactory";
import BooleanEntity from "./BooleanEntity";

class SlotValidator {

    /**
     * Returns an array with all the errors found during the validation.
     * If the returned array is empty, the validation was successful
     *
     * @param slot
     * @param entity
     * @returns {Array}
     */
    validate(slot, entity) {
        let errors = [];

        if (! slot.rules) {
            return errors;
        }

        let validationRules = slot.rules;
        validationRules.forEach(rule => {
            let ruleParams = rule.split("|"),
                ruleHead = ruleParams.shift();
            // this will call the corresponding validate_ function with the slot as first parameter and ARRAY
            // representing each parameter as second parameter
            let pass = this[`validate_${ruleHead}`](entity, ruleParams);

            if (!pass) {
                errors.push(ruleHead);
            }
        });

        return errors;
    }

    validate_inset(entity, params) {
        return params[0].split(",").indexOf(entity.value.toLowerCase()) >= 0;
    }

    validate_future(entity) {
        let aMoment = moment.utc(entity.value);
        return aMoment.isAfter(moment.utc(), "hour");
    }

    /**
     *
     * @param entity
     * @param accuracy
     * @returns {boolean}
     */
    validate_minAccuracy(entity, accuracy) {
        // TODO:

        return true;
        return entity.accuracy && entity.accuracy.indexOf(accuracy) >= 0;
    }

    validate_regex(entity, params) {
        return entity.value.match(new RegExp(params[0], "i"));
    }

}

class SlotFiller {

    // Here is all the knowledge about filling $slot using a static value
    static fillStatic(slot, value) {
        if (slot.type == "value") {
            slot.instance = SlotFactory.create(slot.type, { value: value });
        }

        if (slot.type == "location") {
            slot.instance = SlotFactory.create(slot.type, { value: value });
        }

        if (slot.type == "boolean") {
            slot.instance = SlotFactory.create(slot.type, { value: value });
        }
    }

    static fill(slot, entity) {
        // knowledge about filling $slot using $entity
        if (slot.type == "value") {
            slot.instance = SlotFactory.create("value", { value: entity.value });
        }

        if (slot.type == "location") {
            SlotFiller.fillLocation(slot, entity);
        }

        if (slot.type == "date-interval") {
            SlotFiller.fillDateInterval(slot, entity);
        }

        if (slot.type == "boolean") {
            SlotFiller.fillBoolean(slot, entity);
        }
    }

    static fillBoolean(slot, entity) {

    }

    /**
     * When the slot requires the DateInterval type, it is filled using a combination of datetime and interval entities coming back from
     * the parser. The rules for filling are as follows, and my be overridden in customized bots.
     * - if its a "datetime" entity, then a DateInterval is created based on the entity.accuracy value. EG: if its "day",
     * an interval XX/YY/ZZZZ 00:00 => X+1/YY/ZZZZ 00:00:00 is created for the given day. If its "hour", an interval X:00:00 => X+1:00:00, and so on..
     * - if it is a "interval" entity, its easier. Just grab the begin and end attributes and create an interval from it.
     *
     * @param slot
     * @param entity
     */
    static fillDateInterval(slot, entity) {
        if (entity.name == "datetime") {
            let accuracy = entity.accuracyString,
                from,
                to,
                theDate,
                restrictions;
            switch (accuracy) {
                // eg: next week
                case "week":
                    theDate = moment.utc(entity.value);
                    from = theDate.startOf("week");
                    to = theDate.clone().add(1, "week");
                    break;
                // eg: next weekend
                case "weekend,halfday":
                case "weekend":
                    theDate = moment.utc(entity.value);
                    from = theDate;
                    to = theDate.clone().add(2, "days");
                    if (accuracy == "weekend,halfday") {
                        let halfDayRestriction = SlotFiller.getHalfDayRestrictionIdentifier(theDate);
                        restrictions = [{ type: "halfday", value: halfDayRestriction }];
                    }
                    break;
                // eg: on july 4th
                case "month,day":
                // eg: on monday
                case "day":
                    theDate = moment.utc(entity.value);
                    from = theDate.startOf("day");
                    to = theDate.clone().endOf("day");
                    break;
                // eg: july 4th in the afternoon
                case "month,day,halfday":
                // eg: tomorrow at night/afternoon/midday/etc
                // eg: next friday in the evening
                case "day,halfday":
                    theDate = moment.utc(entity.value);
                    let interval = SlotFiller.makeHalfDayIntervalFromDate(theDate);
                    from = interval.from;
                    to = interval.to;
                    break;
                // eg: in july
                case "month":
                // eg: in july in the morning
                case "month,halfday":
                    theDate = moment.utc(entity.value);
                    from = theDate.startOf("day");
                    to = theDate.clone().startOf("day").add(1, "months");

                    if (accuracy == "month,halfday") {
                        let halfDayRestriction = SlotFiller.getHalfDayRestrictionIdentifier(theDate);
                        restrictions = [{ type: "halfday", value: halfDayRestriction }];
                    }
                    break;
                // eg: on july 4th at 4pm
                // eg: tomorrow at 6pm
                case "day,halfday,hour,min":
                case "month,day,halfday,hour":
                case "month,day,halfday,hour,min":
                    theDate = moment.utc(entity.value);
                    from = theDate;
                    to = theDate.clone();
                    break;
                case "halfday,hour,min":
                    // TODO:
                    break;
                case "halfday":
                    theDate = moment.utc(entity.value);
                    from = theDate.clone().startOf("day");
                    to = theDate.clone().endOf("day");
                    let halfDayRestriction = SlotFiller.getHalfDayRestrictionIdentifier(theDate);
                    restrictions = [{ type: "halfday", value: halfDayRestriction }];
            }

            slot.instance = SlotFactory.create("date-interval", {
                from: from,
                to: to,
                restrictions: restrictions
            });
        } else {
            // TODO: entity name could be Interval instead..
        }
    }

    static makeHalfDayIntervalFromDate(momentDate) {
        let halfDayIdentifier = SlotFiller.getHalfDayRestrictionIdentifier(momentDate);
        switch (halfDayIdentifier) {
            case "morning":
                return { from: momentDate.clone().hour(6), to: momentDate.clone().hour(13) };
            case "afternoon":
                return { from: momentDate.clone().hour(13), to: momentDate.clone().hour(18) };
            case "evening":
                return { from: momentDate.clone().hour(18), to: momentDate.clone().hour(21) };
            case "night":
                return { from: momentDate.clone().hour(21), to: momentDate.clone().add(1, "day").hour(6) };
        }
    }

    /**
     * morning => 06 <= X < 13
     * afternoon => 13 <= X < 18
     * evening => 18 <= X < 21
     * night => 21 <= X <= 23 || 00 <= X < 6
     * @param momentDate
     */
    static getHalfDayRestrictionIdentifier(momentDate) {
        let dateObject = momentDate.toObject();
        if (_.inRange(dateObject.hours, 6, 13)) {
            return "morning";
        } else if (_.inRange(dateObject.hours, 13, 18)) {
            return "afternoon";
        } else if (_.inRange(dateObject.hours, 18, 21)) {
            return "evening";
        } else if (_.inRange(dateObject.hours, 21, 24) || _.inRange(dateObject.hours, 0, 6)) {
            return "night";
        }
    }

    static fillLocation(slot, entity) {
        slot.instance = new LocationSlot(entity.value, {
            value: entity.value,
            type: entity.type || false
        });
    }
}

class Slot {

    constructor(value) {
        this._value = value || false;
        this._valid = true;
        this._errors = [];
        this._dirty = false;
    }

    get errors() {
        return this._errors;
    }

    set errors(errors) {
        this._errors = errors;
    }

    get valid() {
        return this._valid;
    }

    set valid(val) {
        this._valid = val;
    }

    get value() {
        return this._value;
    }

	set value(val) {
		this._dirty = true;
		this._value = val;
	}

    isCompleted() {
        return !!this._value;
    }

    toString() {
        return this.value;
    }

    reset() {
        this._value = false;
    }
}

class LocationSlot extends Slot {

    constructor(value, data = {}) {
        super(value);
        this._type = data.type || false;
    }

    get type() {
        return this._type;
    }

    reset() {
        super.reset();
        this._type = false;
    }
}

class BooleanSlot extends Slot {

    constructor(value, data = {}) {
        value = value == "true";
        super(value, data);

		this._dirty = true;
    }

	reset() {
    	super.reset();
    	this._dirty = false;
	}

    toString() {
        if (this.value) {
            return "Yes";
        } else {
            return "No";
        }
    }

	isCompleted() {
		return this._dirty;
	}

}

class DateSlot extends Slot {

    constructor(value, data = {}) {
        super(value);
        // moment
        this._from = data.from || false;
        // moment
        this._to = data.to || false;
        this._restrictions = data.restrictions || false;
    }

    get value() {
        return {
            from: this._from,
            to: this._to
        };
    }

    isCompleted() {
        return this._from && this._to;
    }

    /**
     * @returns {boolean|moment}
     */
    get from() {
        return this._from;
    }

    /**
     * @returns {boolean|moment}
     */
    get to() {
        return this._to;
    }

    get restrictions() {
        return this._restrictions;
    }

    reset() {
        super.reset();
        this._to = false;
        this._from = false;
        this._restrictions = false;
    }

    toString() {
        let output = "";

        if (!this.from || !this.to) {
            return output;
        }

        output += `from ${this.from.format()} to ${this.to.format()}`;

        if (this.restrictions) {
            output += " with restrictions: ";
            this.restrictions.forEach(restriction => {
                output += ` [type: ${restriction.type} value: ${restriction.value}] `;
            });
        }

        return output;
    }
}

export { Slot, DateSlot, SlotFiller, SlotValidator, LocationSlot, BooleanSlot };