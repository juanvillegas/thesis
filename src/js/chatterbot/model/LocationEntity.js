import Entity from "./Entity";

export default class LocationEntity extends Entity {

    constructor(name, value, data = []) {
        super(name, value, data);

        this._lat = data.lat || [];
        this._lng = data.lng || [];
        this._type = data.type || false;
    }

    get lat() {
        return this._lat;
    }

    get lng() {
        return this._lng;
    }

    get coords() {
        return [this.lat, this.lng];
    }

    get type() {
        return this._type;
    }
}