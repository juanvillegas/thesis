import _ from "lodash";

export default class Memory {

    constructor(memoryRef) {
        this._memoryRef = memoryRef;
        this.memset("contexts", []);
    }

    memset(path, value) {
        _.set(this._memoryRef, path, value);
    }

    memget(path, defaultValue = false) {
        return _.get(this._memoryRef, path, defaultValue);
    }

    memdel(path) {
        _.unset(this._memoryRef, path);
    }

    /**
     * Proxy for learnFact
     * @param path
     * @param value
     */
    learn(path, value) {
        this.learnFact(path, value);
    }

    /**
     * Lears something. Overrides existing content
     * @param path
     * @param value
     */
    learnFact(path, value) {
        _.set(this._memoryRef.facts, path, value);
    }

    /**
     *
     * @param path
     * @param defaultValue
     */
    getFact(path, defaultValue = false) {
        return _.get(this._memoryRef.facts, path, defaultValue);
    }

    /**
     *
     * @param topicText
     */
    hasTopicBeenUsed(topicText) {
        let exists = _.filter(this._memoryRef.topics, t => {
            return t.text == topicText;
        });

        return exists.length > 0;
    }

    /**
     * Adds a topic to the memory. If the topic already exists, nothing happens
     * @param topic
     */
    addTopic(topic) {
        if (! this.hasTopicBeenUsed(topic.text)) {
            this._memoryRef.topics.push(topic);
        }
    }

    /**
     * Adds an array of topics to the memory
     * @param topicArray
     */
    addTopics(topicArray) {
        _.each(topicArray, t => {
            this.addTopic(t);
        });
    }

    hasContext(name) {
        let f = this.memget("contexts", []).filter(ctx => ctx.name == name);
        return f.length > 0;
    }

    hasContextOfType(contextType) {
        let f = this.memget("contexts", []).filter(ctx => ctx.type == contextType);
        return f.length > 0;
    }

    pushContext(ctx) {
        this.memget("contexts", []).push(ctx);
    }

    removeContext(name) {
        this.memset("contexts", this.memget("contexts", []).filter(c => {
            if (c.name == name) {
                c.unregister();
                return false;
            }
            return true;
        }));
    }

    getContext(name) {
        let filtered = this.memget("contexts", []).filter(ctx => ctx.name == name);
        return filtered.length > 0 ? filtered[0] : false;
    }
}

export class MemoryContext {

    constructor(name, memoryRef, data) {
        this._name = name;
        this.memory = memoryRef;
    }

    get name() {
        return this._name;
    }

    /**
     * registers the context in the memory
     */
    register() {
        this.memory.pushContext(this);
    }

    unregister() {
        // TODO: do something when unregistering?
    }

    static construct(memoryType, name, memoryRef, data = []) {
        switch (memoryType) {
            case "select-options":
                return new OptionsMemoryContext(name, memoryRef, data);
                break;
        }
    }

}

export class OptionsMemoryContext extends MemoryContext {

    constructor(name, memoryRef, data) {
        super(name, memoryRef, data);

        this._index = -1;
        this._poolName = data[0];


        // initialize the options pool to an empty set
        this.memory.memset(this._poolName, []);
    }

    get type() {
        return "select-options";
    }

    getOption(index) {
        return this.memory.memget(this._poolName)[index];
    }

    resetOptions() {
        this._index = -1;
    }

    getActiveOption() {
        return this.getOption(this._index);
    }

    setOptions(options) {
        this.memory.memset(this._poolName, options);
    }

    next() {
        this._index++;
    }

    hasMore() {
        return this._index < this.memory.memget(this._poolName).length;
    }
}