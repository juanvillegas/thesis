import _ from "lodash";
import BBDEngine from "./bbd-engine";
import StandardMatchers from "./standard-matchers";
import StandardReplacers from "./standard-replacers";

export default class UtteranceProcessor {

    /**
     * By default the heuristic to choose an utterance is:
     * - filters the set using the "conditions" property of each object, if it was set
     * - sorts the remaining sets. Those that contain already spoken topics gets the lower priority.
     *
     * @param chatterbot
     * @param utterances
     */
    static chooseUtterance(chatterbot, utterances) {
        // 1) filter the utterances by looking at their guard conditions, if any
        let validUtterances = _.filter(utterances, anUtterance => {
            if (anUtterance.conditions) {
                return BBDEngine.matchConditions(chatterbot, anUtterance.conditions);
            }

            anUtterance.score = 1;

            return true;
        });


        let usedUtterances = chatterbot.getMemory("used_utterances", []);

        // 2) assign weights to utterances by looking at their properties and usage statistics collected by the bot
        _.each(validUtterances, anUtterance => {
            // 2.0) apply a slight variation to the score to ensure randomness
            anUtterance.score = anUtterance.score * _.random(0,0.10);

            // 2.1) has the utterance already been used?
            let used = _.reduce(usedUtterances, (accumulator, utterance) => {
                return accumulator || (utterance.id == anUtterance.id);
            }, false);

            if (used) {
                // the utterance has been used => reduce it's score by half
                anUtterance.score = anUtterance.score / 2;
            }

            // 2.2) check if the topics it "talks about" have already been discussed previously
            // for each topic already used, reduce the score by 10%
            let discussedTopics = chatterbot.getMemory("topics", []);
            if (anUtterance.topics) {
                _.each(anUtterance.topics, testTopic => {
                    let haveWeTalkedAboutThis = _.find(discussedTopics, (topic) => {
                        return topic.text == testTopic.text;
                    });

                    if (haveWeTalkedAboutThis) {
                        anUtterance.score = anUtterance.score * 0.9;
                    }
                });
            }
        });

        let selected = _.orderBy(validUtterances, ["score"], ['desc']);

        return selected[0];
    }


    /**
     * If the utterance is transformed, modifications are applied in-place to the Utterance object
     * @param chatterbot
     * @param utterance
     */
    static bakeUtterance(chatterbot, utterance) {
        // apply replacers
        if (utterance.replacers) {
            utterance.text = StandardReplacers.replace(chatterbot, utterance.text, utterance.replacers);
        }
    }

}