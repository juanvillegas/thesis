export default class WebBotCom {

    constructor() {
        this._hears = {};
    }

    bind(element) {
        element.botListens = (val) => {
            this.trigger("utterance", { _utterance: val });
        };
    }

    on(event, handler) {
        this._hears[event] = handler;
    }

    trigger(event, params) {
        if (this._hears[event]) {
            this._hears[event](params);
        }
    }

    speak(utterance) {
        window.document.dispatchEvent(new CustomEvent("chatterbot.speak", { detail: {
            utterance: utterance
        }}));
    }



    report() {
        window.document.dispatchEvent(new Event("chatterbot.refresh"));
    }
}