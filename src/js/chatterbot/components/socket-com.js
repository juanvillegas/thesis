export default class SocketCom {

    constructor(socket) {
    	this._socket = socket;
        this._hears = {};

        this.bind();
    }

    bind() {
    	this._socket.on("user.utterance", (data) => {
			console.log("[BotCom] Received 'user.utterance'");
            this.trigger("utterance", { _utterance: JSON.parse(data).utterance });
		});

    	this._socket.on("user.tutor.spell-check", input => {
			console.log("[BotCom] Received 'user.tutor.spell-check'");
            this.trigger("spell-check", { input: input });
		});

    	this._socket.on("user.tutor.get-suggestions", (input) => {
			console.log("[BotCom] Received 'user.tutor.get-suggestions'");
			this.trigger("get-suggestions", { input: input });
		});

        /**
         * event is an integer representing one of the score events described in Tutor class
         */
    	this._socket.on("user.tutor.score-event", event => {
			console.log("[BotCom] Received 'user.tutor.score-event' with event: " + event);
			this.trigger("score-event", { event: event });
		});
    }

	/**
	 * Registers an event to the Communicator
	 *
	 * @param event
	 * @param handler
	 */
	on(event, handler) {
        this._hears[event] = handler;
    }

	/**
	 * Fires an event from the Communicator
	 * @param event
	 * @param params
	 */
	trigger(event, params) {
        if (this._hears[event]) {
            this._hears[event](params);
        }
    }

    send(event, data) {
		if (event != "chatterbot.refresh") {
			console.log("[BotCom] Emit " + event);
		}

		this._socket.emit(event, data);
	}

    speak(utterance, dictionary) {
		this.send("chatterbot.speak", {
			utterance: utterance,
			dictionary: dictionary
		});
    }

    switchTurn(turn) {
		this.send("chatterbot.switch_turn", turn);
	}

	report(data) {
		this.send("chatterbot.report", data);
    }

    tutorSuggest(suggestions) {
		this.send("chatterbot.tutor.suggest", suggestions);
	}

	tutorSpellCheck(results) {
		this.send("chatterbot.tutor.spell-check-results", results);
	}
}