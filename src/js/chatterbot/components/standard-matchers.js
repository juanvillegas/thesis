import _ from "lodash";
import { SlotValidator } from "../model/Slots";

export default class StandardMatchers {

    /**
     * Compares the detected intent in the last user utterance with the "targetIntent"
     * @param {Chatterbot} chatterbot
     * @param targetIntent
     */
    static compareIntent(chatterbot, targetIntent) {
        let utteranceDescriptor = _.last(chatterbot._utteranceStack);
        if (! Array.isArray(targetIntent)) {
            targetIntent = [targetIntent];
        }

        let mainIntent = utteranceDescriptor.getMainIntent();
        return _.includes(targetIntent, mainIntent.name);
    }

    /**
     * Returns true if the chatterbot hasn't yet fulfilled the given entities
     * @param chatterbot
     * @param entities
     * @returns {boolean}
     */
    static needsEntity(chatterbot, ...entities) {
        return !StandardMatchers.hasEntity(chatterbot, ...entities);
    }

    /**
     * Returns true if the chatterbot has already fulfilled the given entities
     * @param chatterbot
     * @param entities
     * @returns {*}
     */
    static hasEntity(chatterbot, ...entities) {
        // TODO: hack to prevent destructuring from messing up the function.
        // sometimes, the entities get passed as an array, thus using the ... operator creates an array of array...
        if (Array.isArray(entities[0])) {
            entities = entities[0];
        }

        return _.reduce(entities, (acc, entityName) => {
            return acc && chatterbot._isEntityFulfilled(chatterbot._currentGoal.name, entityName);
        }, true);
    }

    static invalidEntity(chatterbot, goalName, slotName) {
        return StandardMatchers.validEntity(chatterbot, goalName, slotName);
    }

    static validEntity(chatterbot, goalName, slotName) {
        let slot = chatterbot.getSlot(goalName, slotName);

        let validator = new SlotValidator();
        return validator.validate(slot, slot.instance).length == 0;
    }

    static hasValidationErrors(chatterbot, slotName) {
        let validationErrors = chatterbot.getMemory("validation.last", false);

        if (validationErrors) {
            let filter = _.filter(validationErrors, error => {
                return error.slot == slotName;
            });

            if (filter.length > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns true if the bot hasn't spoken yet.
     * @param chatterbot
     * @returns {boolean}
     */
    static isFirstUtterance(chatterbot) {
        return chatterbot.getMemory("used_utterances").length == 0;
    }
    /**
     * Returns true if the user hasn't spoken yet.
     * @param chatterbot
     * @returns {boolean}
     */
    static isFirstUserUtterance(chatterbot) {
        return chatterbot.getMemory("last_user_utterance") !== false;
    }

    /**
     *
     * @param chatterbot
     * @param {int} odds Must be a number between 1 and 100. Represents the possibility of the event to happen
     * @returns {boolean}
     */
    static dice(chatterbot, odds) {
        return _.random(1, 100) <= odds;
    }

    static isContextEmpty(chatterbot, contextName) {
        let ctx = chatterbot.MEMORY.getContext(contextName);
        return !ctx.hasMore();
    }

}