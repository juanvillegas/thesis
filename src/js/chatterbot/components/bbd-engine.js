import _ from "lodash";
import BotLog from "../utils/bot-log";
import StandardActions from "./standard-actions";
import StandardMatchers from "./standard-matchers";
import FlightReservationActions from "../../scenarios/flight-reservation/components/flight-reservation-actions";
import FlightReservationMatchers from "../../scenarios/flight-reservation/components/flight-reservation-matchers";

export default class BBDEngine {

    /**
     * @param {Chatterbot} chatterbot
     * @param {array} conditions
     */
    static matchConditions(chatterbot, conditions) {
        let matches = true;

        _.each(conditions, condition => {
            matches = matches && BBDEngine.executeCondition(condition, chatterbot);
        });

        return matches;
    }

    static matchStateActions(chatterbot, actionGroups) {
        return _.filter(actionGroups, actionGroup => {
            if (actionGroup.condition) {
                return BBDEngine.matchConditions(chatterbot, actionGroup.condition);
            }
            // if no condition, just match
            return true;
        });
    }

    static executeCondition(conditionString, chatterbot) {
        let args = BBDEngine._getActionStringArgs(conditionString),
            functor = args.shift(),
            negatedFunctor = functor.startsWith("!");

        if (negatedFunctor) {
            functor = functor.substr(1);
        }

        let functorParts = functor.split(".");

        args = BBDEngine._parseArgs(args);

        let functionName = _.camelCase(functorParts[1]),
            conditionResult;
        switch (functorParts[0]) {
            case "std":
                conditionResult = StandardMatchers[functionName](chatterbot, ...args);
                break;
            case "fr":
                conditionResult = FlightReservationMatchers[functionName](chatterbot, ...args);
                break;
            default:
                let theError = "[BBD] No match for the requested namespace: " + functorParts[0];
                chatterbot.log(theError);
                throw theError;
        }

        if (negatedFunctor) {
            conditionResult = !conditionResult;
        }

        chatterbot.log("[BBD] Executed condition " + conditionString + " with output: " + (conditionResult ? "true" : "false"));

        return conditionResult;
    }

    static _parseArgs(args) {
        let parsedArgs = [];

        _.each(args, (argVal) => {
            // is it [arg1,arg2,...,argn] form?
            let isArrayRegEx = /^\[(.+)\]$/ig,
                matches = isArrayRegEx.exec(argVal);
            if (matches) {
                matches.shift();
                parsedArgs.push(matches[0].split(","));
            } else {
                // else, it is just a string
                parsedArgs.push(argVal);
            }

        });

        return parsedArgs;
    }

    static executeAction(actionString, chatterbot, previousResponse = false) {
        return new Promise((resolve, reject) => {
            let args = BBDEngine._getActionStringArgs(actionString),
                functor = args.shift(),
                functorParts = functor.split(".");

            args = BBDEngine._parseArgs(args);

            let functionName = _.camelCase(functorParts[1]);

            // TODO: reemplazar estos nombres de clases hardcodeados por un mecanismo que permita registrar Acciones dinamicamente.
            switch (functorParts[0]) {
                case "std":
                    if (StandardActions[functionName]) {
                        resolve(StandardActions[functionName](chatterbot, previousResponse, ...args));
                    } else {
                        BotLog.logError(`[BBD][STD] Transition functor: "${functionName}" couldn't be found in class and thus wasn't executed.`);
                        reject(false);
                    }
                    break;
                case "fr":
                    if (FlightReservationActions[functionName]) {
                        resolve(FlightReservationActions[functionName](chatterbot, previousResponse, ...args));
                    } else {
                        BotLog.logError(`[BBD][FR] Transition functor: "${functionName}" couldn't be found in FlightReservationActions class and thus wasn't executed.`);
                        reject(false);
                    }
                    break;
            }
        });
    }

    static _getActionStringArgs(inputString) {
        // by default was: inputString.split(",")

        // now we need to support functor,(arg1),(arg2) as well, with variable arguments...
        // [\w-\.]+,\(([\w,]+)\),\(([\w,]+)\)
        let isCompoundRegEx = /[\w-\.]+,\([\w-\.]+\)*/ig;
        // array parameters
        let isArrayRegEx = /[\w-\.]+,\[[\w-\.]+\]*/ig;

        if (isCompoundRegEx.test(inputString)) {
            // complex format str,(arg),(arg2),...
            // TODO: we could possibly match any number of arguments here by iterating until a match is found, with a hard cap limit...
            let iteration = 0,
                matches = false,
                regexes = [
                    /([\w\-\.]+),\(([\w\.\-\[\],]+)\),\(([\w\.\-\[\],]+)\)/ig,
                    // /([\w\-\.]+),\(([\w\.\-\[\],]+)\),\(([\w\.\-\[\],]+)\)/ig,
                    /([\w\-\.]+),\(([\w\.\-\[\],]+)\)/ig
                ];
            while (!matches && iteration < regexes.length) {
                matches = regexes[iteration].exec(inputString);
                iteration++;
            }

            matches.shift();
            // compound format: str, [!] arg1, arg2...
            return matches;
        } else if (isArrayRegEx.test(inputString)) {
            // str, [...arrVals]
            let iteration = 0,
                matches = false,
                regexes = [
                    /([\w\-\.]+),(\[[\w\.\-\[\],]+\])/ig
                ];
            while (!matches && iteration < regexes.length) {
                matches = regexes[iteration].exec(inputString);
                iteration++;
            }

            matches.shift();
            return matches;
        } else {
            // simple format str,arg1,arg2,argN...
            return inputString.split(",");
        }
    }

}