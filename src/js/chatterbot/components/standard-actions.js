import _ from "lodash";
import StandardReplacers from "./standard-replacers";
import Chatterbot from "../chatterbot";
import BBDEngine from "./bbd-engine";
import Utterance from "../model/Utterance";
import UtteranceProcessor from "./utterance-processor";
import UtteranceDescriptor from "../analysis/utterance-descriptor";
import { SlotFiller, SlotValidator } from "../model/Slots";
import { MemoryContext } from "./memory";

export default class StandardActions {

    static activateGoal(chatterbot, previousResponse, goalName) {
        _.each(chatterbot._allGoals, (g) => {
            if (g.name == goalName) {
                g.active = true;
            }
        });

        chatterbot.report();
    }

    static resetGoal(chatterbot, previousResponse, goalName) {
        let resetList = [];

        _.each(chatterbot._allGoals, (g) => {
            if (g.name == goalName) {
                resetList.push(g);
            } else if (g.parent == goalName) {
                resetList.push(g);
            }
        });

        _.each(resetList, g => {
            if (g.slots) {
                _.each(g.slots, goalSlot => {
                    goalSlot.instance.reset();
                });
            }
            g.solved = false;
        });

        chatterbot.report();
    }

    static deactivateGoal(chatterbot, previousResponse, goalName) {
        _.each(chatterbot._allGoals, (g) => {
            if (g.name == goalName) {
                g.active = false;
            }
        });

        chatterbot.report();
    }

    static goToState(chatterbot, previousResponse, stateName) {
        // clear action queue
        chatterbot._actionsQueue = [];
        chatterbot.pushGoalState(stateName);
    }

    static solveGoal(chatterbot, previousResponse, goalName) {
        let parentCheckList = [];

        _.each(chatterbot._allGoals, (g) => {
            if (g.name == goalName) {
                g.solved = true;

                if (g.onSolved) {
                    _.each(g.onSolved, actionString => {
                        BBDEngine.executeAction(actionString, chatterbot, false);
                    });
                }

                if (g.parent) {
                    parentCheckList.push(g.parent);
                }
            }
        });

        _.each(parentCheckList, parentGoal => {
            let pending = _.filter(chatterbot._allGoals, childGoal => {
                return childGoal.parent == parentGoal && !childGoal.solved;
            });

            if (pending.length == 0) {
                StandardActions.solveGoal(chatterbot, false, parentGoal);
            }
        });

        chatterbot.report();
    }

    /**
     * Speaks an utterance from the given utterance set
     * @param chatterbot
     * @param previousResponse
     * @param utteranceSetId
     * @constructor
     */
    static speak(chatterbot, previousResponse, utteranceSetId) {
        // chooses an utterance from the set, by looking at some of its properties: spoken topics, matching conditions, etc
        let utterance = UtteranceProcessor.chooseUtterance(chatterbot, chatterbot._UDB[utteranceSetId]);

        chatterbot._memory.last_system_utterance = utterance;
        chatterbot._memory.used_utterances.push(utterance);

        if (utterance.topics) {
            chatterbot.MEMORY.addTopics(utterance.topics);
        }

        // transforms the utterance: applies replacers, etc.
        UtteranceProcessor.bakeUtterance(chatterbot, utterance);

        // Tutor: dictionary
		utterance.dictionary = chatterbot.tutor.buildDictionary(utterance.text);

        chatterbot.speak(utterance);
    }

    static speakCustom(chatterbot, previousResponse, utteranceString) {
        let anUtterance = {
            text: utteranceString
        };

        // transforms the utterance: applies replacers, etc.
        UtteranceProcessor.bakeUtterance(chatterbot, anUtterance);

        chatterbot.speak(anUtterance);
    }

    static goToStep(chatterbot, previousResponse, stepName) {
        console.log("going to step " + stepName);
    }

	/**
	 * Listens for User input
	 * The bot explicitly switch the current turn twice, once before listening and once after input's
	 * received.
	 *
	 * @param chatterbot
	 * @returns {Promise}
	 */
	static listen(chatterbot) {
	    chatterbot.deleteMemory("validation.last");

		chatterbot.switchTurn();
        return new Promise((resolve, reject) => {
            chatterbot._BotCom.on("utterance", (params) => {
                let userUtterance = UtteranceDescriptor.createFromString(params._utterance);
                chatterbot._utteranceStack.push(userUtterance);
                chatterbot._memory.last_user_utterance = userUtterance;
                chatterbot.switchTurn();
                resolve(userUtterance);
            });
        });
    }

    static processUtterance(chatterbot) {
		// When the user speaks something, the chatbot registers the utterance in the Queue.
		// Now we will attempt to "decorate" that same utterance, which is the last one, with proper intents, entities, etc.
        let utteranceDescriptor = _.last(chatterbot._utteranceStack);

        let analyzer = chatterbot.analyzer;
        return analyzer.analyze().then(analyzedUtterance => {
            UtteranceDescriptor.merge(utteranceDescriptor, analyzedUtterance);

            chatterbot.handleSmallTalk();

            return utteranceDescriptor;
        });
    }

    /**
     * Updates the given slotName[entityName] by reading the same entity from the last utterance, if any is available.
     * @param chatterbot
     * @param previousResponse
     * @param slotGroup
     * @param slotName
     */
    static updateSlot(chatterbot, previousResponse, slotGroup, slotName) {
        let matchingEntities = StandardActions.getMatchingEntities(chatterbot, previousResponse, slotGroup, slotName);

        matchingEntities.forEach(entity => {
            SlotFiller.fill(slot, entity);
        });

        chatterbot.report();
    }

    static getMatchingEntities(chatterbot, previousResponse, slotGroup, slotName) {
        let retVal = [];

        let utteranceDescriptor = _.last(chatterbot._utteranceStack);

        let slot = chatterbot.getSlot(slotGroup, slotName);
        if (!slot) {
            chatterbot.log("No slot with name " + slotName + " was found in the slot group: " + slotGroup);
            return;
        }

        let matchingIntents;
        if (slot.intents.indexOf("any") >= 0) {
            matchingIntents = ["any"];
        } else {
            matchingIntents = utteranceDescriptor._intents.filter(intent => intent.name == "any" || slot.intents.indexOf(intent.name) >= 0);
        }

        if (matchingIntents.length > 0) {
            let utteranceEntities = utteranceDescriptor.getEntities();
            retVal = utteranceEntities.filter(ent => slot.entities.indexOf(ent.name) >= 0);
        }

        return retVal;
    }

    static setSlot(chatterbot, previousResponse, slotGroup, slotName, slotValue) {
        let replaced = StandardReplacers.replace(chatterbot, slotValue, []); // TODO: wtf?

        let slot = chatterbot.getSlot(slotGroup, slotName);

        if (slot !== false) {
            SlotFiller.fillStatic(slot, slotValue);
        }

        chatterbot.report();
    }

    static fillSlot(chatterbot, previousResponse, slotGroup, slotName) {
        let slot = chatterbot.getSlot(slotGroup, slotName);
        if (!slot) {
            chatterbot.log(`[Chatterbot::fillSlot] Slot <${slotGroup},${slotName}> not found. Aborting.`);
            return;
        }

        /*if (slot.type == "boolean") {
            // Boolean slots can be filled directly from the UtteranceDescriptor, by looking into the true/false intents

        }*/

        let matchingEntities = StandardActions.getMatchingEntities(chatterbot, previousResponse, slotGroup, slotName);

        let validationErrors = [];
        let validator = new SlotValidator();
        let validEntities = matchingEntities.filter(entity => {
            let errors = validator.validate(slot, entity);

            if (errors.length == 0) {
                return true;
            } else {
                validationErrors.push({ slot: slot.name, name: entity.name, errors: errors });
            }
        });

        if (validationErrors.length > 0) {
            chatterbot.log("[Chatterbot::fillSlot] Slot not filled due to validation errors.");
            chatterbot.setMemory("validation.last", validationErrors);
        } else {
            validEntities.forEach(entity => {
                SlotFiller.fill(slot, entity);

                // slot is an instance of Slot class
                chatterbot.log("[Chatterbot::fillSlot] Filling slot " + slot.name + " with " + slot.instance.toString());
            });
            chatterbot.tutor.validateRestrictions();
        }

        chatterbot.report();
    }

    static refreshGoals(chatterbot, previousResponse, ...goals) {
        goals.forEach(goal => {
            if (chatterbot._isGoalCompleted(goal)) {
                StandardActions.solveGoal(chatterbot, previousResponse, goal);
            }
        });

        chatterbot.report();
    }

    static ifThen(chatterbot, previousResponse, condition, action) {
        if (BBDEngine.executeCondition(condition, chatterbot)) {
            BBDEngine.executeAction(action, chatterbot);
        }
    }

    static confirm(chatterbot, previousResponse, utteranceSet = false) {
        // TODO: ?
    }

    static openContext(chatterbot, previous, contextType, contextName, ...data) {
        let ctx = MemoryContext.construct(contextType, contextName, chatterbot.MEMORY, data);
        ctx.register();
    }

    static closeContext(chatterbot, previous, contextName) {
        let ctx = chatterbot.MEMORY.removeContext(contextName);
    }

    static showContextOption(chatterbot, previousResponse, contextName) {
        let ctx = chatterbot.MEMORY.getContext(contextName);

        ctx.next();
        let anOption = ctx.getActiveOption();

        StandardActions.speakCustom(chatterbot, previousResponse, anOption.toString());

        console.log(anOption.toString());
    }

    static end(chatterbot, previousResponse) {
        chatterbot.end();
    }

    static switchTurn(chatterbot, previousResponse) {
    	chatterbot.switchTurn();
	}

	static tutorFinishStep(chatterbot, previousResponse) {
        chatterbot.tutor.awardStep();
        chatterbot.report();
    }

    static report(chatterbot, previousResponse) {
        chatterbot.report();
    }

}