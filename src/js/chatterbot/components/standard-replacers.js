import _ from "lodash";

export default class StandardReplacers {

    /**
     * Applies the replacer functions in the replacersArray array to the given text string
     * @param {Chatterbot} chatterbot
     * @param {string} text
     * @param {array} replacersArray
     */
    static replace(chatterbot, text, replacersArray) {
        let dict = {};

        _.each(replacersArray, (replacer, index) => {
            let pieces = replacer.split("|"),
                functor = pieces[0],
                args = pieces[1].split(",");

            args = _.map(args, (a) => {
                for (let counter = 1; counter <= index; counter++) {
                    a = a.replace(new RegExp("\\$" + counter), dict["$" + counter]);
                }

                return a;
            });

            debugger;
            if (functor == "$slot") {
                dict[`$${index+1}`] = chatterbot.getSlotValue(args[0], args[1]);
            }

            if (functor == "$fact") {
                dict[`$${index+1}`] = _.get(chatterbot._memory.facts, args[0]);
            }
        });

        debugger;
        _.each(dict, (val, key) => {
            text = text.replace(key, val);
        });

        return text;
    }
}