export default class StandardExpanders {

	/**
	 * @param {string} text
	 */
	static expand(text) {
		let regex = new RegExp(/\$(\w+)\(([\w\s,]+)\)/);

		let expanded = [];
		let workingSet = [text];

		while (workingSet.length > 0) {
			let currentText = workingSet[0];
			let matches = currentText.match(regex);

			if (! matches) {
				expanded.push(currentText);
				workingSet.shift();
			} else {
				let functor = matches[1],
					replaceable = matches[0],
					args = matches[2];

				let replacements = ExpanderFunctors.execute(functor, args);
				replacements.forEach((r) => {
					workingSet.push(currentText.replace(replaceable, r));
				});

				workingSet.shift();
			}
		}

		return expanded;
	}
}

class ExpanderFunctors {

	static execute(functor, args) {
		return ExpanderFunctors[functor + "Execute"](args);
	}

	static setExecute(args) {
		return args.split(",");
	}

	static varExecute(args) {
		let theArgument = args.split(",")[0];

		return ["<<" + theArgument + ">>"];
	}

}