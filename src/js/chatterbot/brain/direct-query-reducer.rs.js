export default
`+ (not at all|absolutely not|of course not|no|noo|nooo|ne|nee|neee|nah|naah|nope|nao|naa|naaa) [*]
- { "success": true, "type": "negative" }

+ (of course|absolutely|yes|yeah|yea|ye|si|yep) [*]
- { "success": true, "type": "positive" }

+ * 
- { "success": false }
`;