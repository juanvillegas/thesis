export default
`+ [*] i will [select|choose] the (first|second|third|fourth|fifth|sixth) *
- { "intents": ["select-option"], "entities": [{ "name": "ordinal", "value": "<star1>" }] }

+ [i will select|i will have|i will use|i will take] [the] option number *
- { "intents": ["select-option"], "entities": [{ "name": "ordinal", "value": "<star1>" }] }
`;