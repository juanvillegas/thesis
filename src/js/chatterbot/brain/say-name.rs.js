export default
`+ [*] it is *
- { "success": true, "intents": ["say-name"], "entities": [{ "name": "username", "value": "<star>" }] }

+ [*] my name is *
- { "success": true, "intents": ["say-name"], "entities": [{ "name": "username", "value": "<star>" }] }

+ *
- { "success": true, "intents": ["say-name"], "entities": [{ "name": "username", "value": "<star>" }] }
`;