export default
`+ [*] my [id|identification|national id|dni|identification number] is *
- { "success": true, "intents": ["say-identification"], "entities": [{ "name": "identification", "value": "<star>" }] }

+ [sure|yes|absolutely] it is *
- { "success": true, "intents": ["say-identification"], "entities": [{ "name": "identification", "value": "<star>" }] }

+ *
- { "success": true, "intents": ["say-identification"], "entities": [{ "name": "identification", "value": "<star>" }] }
`;