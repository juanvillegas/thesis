import StandardExpanders from "../components/standard-expanders";
import _ from "lodash";
import levenshtein from "fast-levenshtein";

export default class TutorSuggestions {

	constructor(data) {
		this._data = data;

		this.MAX_MATCHING_SUGGESTIONS = 5;
		this.MAX_OPTIMAL_SUGGESTIONS = 2;
		this.MAX_SUGGESTION_DISTANCE = 40;
	}

	getSuggestions() {
		let processedSuggestions = [];
		let rawSuggestions = _.extend({}, this._data.lastUtterance.suggestions);

		// TODO: add suggestions based on query type (at least yes,no for binary queries)

		if (! rawSuggestions) {
			return [];
		}

		// 1: apply expanders
		_.each(rawSuggestions, s => {
			processedSuggestions = _.union(processedSuggestions, StandardExpanders.expand(s));
		});

		// 2: sort by similarity to input
		processedSuggestions = processedSuggestions.map(suggestion => {
			let suggestionData = {
				value: "",
				score: 0,
				top: false
			};

			if (suggestion.charAt(0) == "*") {
				suggestionData.top = true;
				suggestion = suggestion.substring(1);
			}

			let input = this._data.input.toLowerCase();
			let comparable = suggestion;
			comparable = comparable.replace(/(<<\w+>>)/, "").replace("  ", " ").toLowerCase();

			let distance = levenshtein.get(comparable, input);
			if (input.length < comparable.length) {
				if (comparable.substr(0, input.length) == input) {
					distance = distance / 3;
				}
			}

			suggestionData.value = suggestion;
			suggestionData.score = distance;

			return suggestionData;
		});

		// 3. sort using the score calculated above. lowest scores go first
		processedSuggestions.sort((a, b) => {
			if (a.score < b.score) {
				return -1;
			}

			if (a.score == b.score) {
				return 0;
			}

			return 1;
		});

		// lets filter out scores > 40
		processedSuggestions = processedSuggestions.filter(s => s.score <= this.MAX_SUGGESTION_DISTANCE);
		processedSuggestions = processedSuggestions.slice(0, this.MAX_MATCHING_SUGGESTIONS);

		return {
			top: processedSuggestions.filter(s => s.top).map(s => s.value),
			matching: processedSuggestions.map(s => s.value),
		};
	}

}