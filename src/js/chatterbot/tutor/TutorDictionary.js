import _ from "lodash";

export default class TutorDictionary {

	constructor(dict, utterance) {
		this._dict = dict;
		this._utterance = utterance;
	}

	getDictionary() {
		let words = this._utterance.split(" ");

		let utteranceDictionary = [];
		_.each(words, w => {
            let original = w;
			let index = w.toLowerCase().replace(/\W+/g, "");
			if (this._dict[index]) {
			    this._dict[index].original = original;
				utteranceDictionary.push(this._dict[index]);
			}
		});
		return utteranceDictionary;
	}
}