export default {
	"sets": {
		"thank": [
			"Thank you",
			"Much appreciated",
			"Thank you very much",
			"Thanks a lot",
			"You $set(are,were) so halpful",
			"That $set(was,is) is very kind of you"
		]
	}
}