var TutorSuggestions = require("./TutorSuggestions").default;
var TutorDictionary = require("./TutorDictionary").default;
var TutorSpellChecker = require("./TutorSpellChecker").default;
var _ = require("lodash");

export default class Tutor {

	constructor(chatterbot) {
		this._chatterbot = chatterbot;

		this._score = 0;
		this._stepScore = 0;
		this._scoreUpdates = [];
		this.BASIC_STEP_SCORE = 100;
		this.TUTOR_DISPLAYED_SUGGESTIONS = 1;
		this.TUTOR_DETECTED_SPELL_ERROR = 2;
		this.TUTOR_USED_DICTIONARY = 3;
	}

	setup() {
	    this.resetStepScore();
    }

    awardStep() {
	    this._score += this._stepScore;
	    this.resetStepScore();
    }

	resetScore() {
	    this._score = 0;
    }

	resetStepScore() {
	    this._stepScore = this.BASIC_STEP_SCORE;
    }

    stepEnded() {
	    this._score += this._stepScore;
	    this.resetStepScore();
    }

    getLastScoreUpdate() {
	    return this._scoreUpdates[this._scoreUpdates.length - 1];
    }

	applyScoreUpdate(eventName) {
	    let oldScore = this._stepScore,
            scoreDelta = 0;

	    switch (eventName) {
            case this.TUTOR_DISPLAYED_SUGGESTIONS:
                scoreDelta = oldScore;
                this._stepScore = 0;
                break;
            case this.TUTOR_DETECTED_SPELL_ERROR:
                scoreDelta = 10;
                this._stepScore -= 10;
                break;
            case this.TUTOR_USED_DICTIONARY:
                scoreDelta = 5;
                this._stepScore -= 5;
                break;
        }

        if (this._stepScore < 0) {
	        this._stepScore = 0;
        }

        this._scoreUpdates.push({ before: oldScore, delta: scoreDelta });
    }

	getScore() {
	    return this._score;
    }

    getStepScore() {
	    return this._stepScore;
    }

	buildDictionary(utterance) {
		let instance = new TutorDictionary(this._chatterbot._scenarioDictionary, utterance);
		return instance.getDictionary();
	}

	checkSpelling(utterance) {
		let instance = new TutorSpellChecker(utterance);
		return instance.check();
	}

	suggest(input) {
		let data = {
			lastUtterance: this._chatterbot.getMemory("last_system_utterance"),
			input: input
		};
		let instance = new TutorSuggestions(data);
		return instance.getSuggestions();
	}

    /**
     * Executes the process of validating the restrictions for this scenario. The process basically is:
     * + For each restriction R
     * + Get the corresponding Slot S for R, given <goalName, slotName> tuple
     * + execute R->validate(S)
     * + update "valid" field in corresponding Goal and Slot
     * + calculate error messages by executing R->getInvalidMessages()
     *
     */
    validateRestrictions(restrictions, goals, slots) {
        _.each(goals, g => g.valid = true );

        _.each(restrictions, R => {
            let slot = this._chatterbot.getSlot(R.goal, R.slot);

            if (slot) {
                let validationErrors = R.validate(slot.instance);

                slot.instance.errors = validationErrors;
                slot.instance.valid = validationErrors.length == 0;

                if (!slot.valid) {
                    _.each(goals, g => {
                        if (g.name == R.goal) {
                            g.valid = false;
                        }
                    });
                }
            }
        });
    }

}