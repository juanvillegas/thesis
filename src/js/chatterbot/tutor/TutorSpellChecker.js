import _ from "lodash";
import axios from "axios";

export default class TutorSpellChecker {

	constructor(utterance) {
		this._utterance = utterance;
	}

	/**
	 * NOTICE: returns a promise!
	 *
	 * The promise's results will be:
	 * + the boolean "false" in case an error has occurred (dead api, other error)
	 * + an array with children in the following object structure:
	 * { offset: int, length: int, bad: string, better: array<string> }
	 *
	 * @returns {Promise}
	 */
	check() {
		return this.checkSpellingRemote().then(() => this.formatResponse);
	}

	getRemote() {
		return process.env.TEXTGEARS_REMOTE + "text=" + this._utterance + "&key=" + process.env.TEXTGEARS_API_KEY;
	}

	checkSpellingRemote() {
		return axios.get(this.getRemote()).then((response) => {
			let results = response.data;
			if (results.result) {
				return results.errors;
			}

			return false;
		}, () => {
			return false;
		});
	}

	formatResponse(checkResults) {
		if (checkResults) {
			return checkResults
                .filter(o => {
                    // check ignore list
                    return ! this.shouldIgnore(o.bad.toLowerCase());
                })
                .filter(o => {
                    // accept errors only when there are suggestions AND
                    return o.better.length >= 0 &&
                        // ignore case errors AND
                        o.bad.toLowerCase() != o.better[0].toLowerCase();
                })
                .map(o => {
                    return {
                        offset: o.offset,
                        length: o.length,
                        bad: o.bad,
                        better: o.better
                    };
                });
		}

		return checkResults;
	}

	shouldIgnore(word) {
        return [
            'viedma', 'cordoba', 'florianopolis', 'buenos aires', 'santiago de chile', 'bahia blanca'
        ].indexOf(word) != -1;
    }
}