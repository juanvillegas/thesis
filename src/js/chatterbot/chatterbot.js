import _ from "lodash";
import shortid from "shortid";
import BotLog from "./utils/bot-log";
import BBDEngine from "./components/bbd-engine";
import SlotFactory from "./model/SlotFactory";
import SlotRestriction from "./model/SlotRestriction";
import Memory from "./components/memory";
import Analyzer from "./analysis/analyzer";
import Tutor from "./tutor/Tutor";
import RiveScript from "rivescript";

// RiveScript default brain definitions
import DirectQueryReducerCode from "./brain/direct-query-reducer.rs";
import SelectOptionsCode from "./brain/select-options.rs";
import GenericsCode from "./brain/generics.rs";

export default class Chatterbot {

    /**
     *
     * @param botCom
     * @param {object} botConfig
     *
     * BotConfig is an object that may include:
     * - <array> rsBrains: array containing objects in the following format:
     * { name: <string>, code: <reference to a rivescript brain def> }
     * RiveScript Brains will be "installed" by the bot when it is initialized, and can be referenced by the analyzer
     * and the BBD code, using the brain's name.
	 * - <int> scenarioVariation: the selected variation for the scenario (default: a random one)
     *
     * @param {object} scenarioDescriptor the description object for the scenario to be executed.
     */
    constructor(botCom, botConfig, scenarioDescriptor) {
        this._config = botConfig;

        this.TURN_USER = "USER";
        this.TURN_COM = "COM";

        this._currentTurn = this.TURN_COM;

        this._Log = new BotLog();

        this._sessionId = botConfig.sessionId || _.random(99999999, 99999999999);
        this._scenarioVariation = botConfig.scenarioVariation || false;
		this._scenarioDictionary = {};

        this._BotCom = botCom;

        // GOALS
        this._allGoals = [];
        this._pendingGoals = [];
        this._currentGoal = false;
        this._scenarioRestrictions = [];

        // UTTERANCES DATABASE
        this._UDB = {};

        // The current scenario slots
        this.slots = {
            // slotName => array of slot objects
        };

        /**
         * All the conversation Utterances, with the latest at the bottom (LIFO)
         * @type {Array}
         * @private
         */
        this._utteranceStack = [];

        // Actions Queue
        this._actionsQueue = [];
        this._currentAction = "";

        this._memory = {
            last_system_utterance: false,
            last_user_utterance: false,
            // an array with references to all the utterances that have been used by the bot.
            used_utterances: [],
            // array storing all the topics that have been covered in the conversation
            topics: [],
            // dictionary storing all the things the bot knows. The dictionary is updated as the bot learns new things.
            facts: {}
        };

        this.MEMORY = new Memory(this._memory);

        this._initialize(scenarioDescriptor, this._scenarioVariation);
    }

    _initialize(descriptor, variationIndex) {
    	// TUTOR
		this.tutor = new Tutor(this);
		this.tutor.setup();


        // RIVESCRIPT
        // 1) install default RiveScript brains
        this.rsBrains = {
        	generics: {
        		code: GenericsCode,
				instance: false
			},
            directQueryMatcher: {
                code: DirectQueryReducerCode,
                instance: false
            },
            selectOptions: {
                code: SelectOptionsCode,
                instance: false
            }
        };

        // 1.5) Install brains provided by subclasses, if any
        if (this._config.rsBrains) {
            _.each(this._config.rsBrains, (brain) => {
                this.rsBrains[brain.name] = {
                    code: brain.code,
                    instance: false
                }
            });
        }

        _.each(this.rsBrains, (brain, brainName) => {
            brain.instance = new RiveScript({
                debug: false,
                onDebug: (m) => {
                    console.debug("[Rive: " + brainName + " Matcher] " + m);
                }
            });

            if (brain.instance.stream(brain.code)) {
                brain.instance.sortReplies();
            }
        });

        // install goals
        _.each(descriptor.goals, (goal) => {
            this.slots[goal.name] = [];

            if (goal.slots) {
                this.slots[goal.name] = this._getGoalSlots(goal);
            }

            let clonedGoal = _.clone(goal);

            this._allGoals.push(clonedGoal);
            this._pendingGoals.push(clonedGoal);

            if (goal.default) {
                this._defaultGoal = clonedGoal;
            }
        });

        // install the Utterance Database
        shortid.characters("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-");
        _.each(descriptor.utterances, (utteranceSet) => {
            this._UDB[utteranceSet.id] = [];
            _.each(utteranceSet.values, utterance => {
                utterance.id = shortid.generate();

                utterance.suggestions = _.extend(
                    {},
                    _.get(utteranceSet, "suggestions", []),
                    _.get(utterance, "suggestions", [])
                );

				this._UDB[utteranceSet.id].push(utterance);
            });
        });

        // Get required variation
		variationIndex = variationIndex || _.random(0, descriptor.variations.length - 1);
		var scenarioVariation = descriptor.variations[variationIndex];

		if (!scenarioVariation) {
			BotLog.logError('[Chatbot] Warning, selected variation is invalid (possibly an out of bounds index was provided).');
		}

        // install facts
        _.each(scenarioVariation.facts, (fact) => {
            _.set(this._memory.facts, fact.text, fact.value);
        });

		// install restrictions
		_.each(scenarioVariation.restrictions, (restrictionDefinition) => {
			this._scenarioRestrictions.push(SlotRestriction.createFromDefinition(_.extend({}, restrictionDefinition)));
		});

		if (descriptor.dictionary) {
			this._scenarioDictionary = _.extend({}, descriptor.dictionary);
		}
    }

    execute(params = {}) {
    	// TODO: ?
        this._executionParams = params;

        this._BotCom.on("get-suggestions", data => {
        	let suggestions = this.tutor.suggest(data.input);
        	this._BotCom.tutorSuggest(suggestions);
        	this.report();
		});

        this._BotCom.on("spell-check", data => {
			this.tutor.checkSpelling(data.input).then(results => {
				this._BotCom.tutorSpellCheck(results);
			});
		});

        this._BotCom.on("score-event", data => {
            this.tutor.applyScoreUpdate(data.event);

            this._BotCom.send("chatterbot.score.update", this.tutor.getLastScoreUpdate());
            this.report();
		});

        this.report();
        this.enterStateInit();
    }

    report() {
        this._BotCom.report(this.getReportData());
    }

	getReportData() {
		let data = {};

		this.tutor.validateRestrictions(this._scenarioRestrictions, this._allGoals, this.slots);

		data.goals = {};
		_.each(
			_.filter(this._allGoals, g => g.displayable), (goal) => {
				let goalData = _.pick(goal, ['active', 'parent', 'description', 'invalid', 'name', 'nice', 'solved']);
				goalData.slots = {};

				_.each(goal.slots, s => {
				    goalData.slots[s.name] = {
				        name: s.name,
				        nice: s.nice,
                        value: s.instance.value,
                        errors: s.instance.errors,
                        valid: s.instance.valid
                    };
                });

				data.goals[goal.name] = goalData;
			}
		);

		data.restrictions = _.map(this._scenarioRestrictions, restriction => {
		    return {
		        nice: restriction._nice,
                value: restriction._value,
                goal: restriction._goal,
                slot: restriction._slot
            };
        });

		data.actionQueue = this._actionsQueue;
		data.currentAction = this._currentAction ? this._currentAction : null;

		data.score = this.tutor.getScore();
		data.stepScore = this.tutor.getStepScore();

		return data;
	}

    enterStateInit() {
        BotLog.log("Entered state: init");

        this.enterStateGoalSelect();
    }

    enterStateGoalSelect() {
        BotLog.log("Entered state: goal_select");

        if (this._pendingGoals.length == 0) {
            this.enterStateEnd();
            return;
        }

        this._currentGoal = this._getNextGoal();

        if (this._currentGoal) {
            BotLog.log(`Goal set to ${this._currentGoal.name}`);

            this._currentGoal.active = true;
            this.executeGoal();
        } else {
            BotLog.log("All goals consumed");
        }
    }

    end() {
    	// TODO: ??
        alert("The excersise has ended!");
    }

    _consumeActions(previousResponse = false) {
        if (this._actionsQueue.length == 0) {
            return new Promise((accept, reject) => {
                return accept();
            });
        }

        this._currentAction = this._actionsQueue.shift();

        BotLog.logAction("[Next action] " + this._currentAction);

        return BBDEngine.executeAction(this._currentAction, this, previousResponse)
                .then(actionResponse => {
                    if (this._actionsQueue.length > 0) {
						return this._consumeActions(actionResponse);
                    }
                });
    }

    executeGoal() {
        BotLog.log("entered state: state_execute");

        // TODO: Improvement: "init" doesn't always really have to be the initial state.
        this.pushGoalState("init");

        // TODO: if the actionsQueue here is empty, no condition matched.
        // => we can either mark the exercise as failed..
        // => or mark the goal as fulfilled
        // => or mark the goal as failed

        this._consumeActions().then(() => {
            BotLog.log("[BBD] Actions Queue is now empty.");

            this.enterStateGoalSelect();
        });
    }

    /**
     * Satellite method connecting to underlying implementation
     * @param utterance
     */
    speak(utterance) {
        this._BotCom.speak(utterance.text, utterance.dictionary);
        BotLog.speech(utterance.text);
    }

	/**
	 * Just a proxy for isEntityFulfilled
	 * @param goalName
	 * @param slotName
	 * @returns {*}
	 * @private
	 */
    _isSlotCompleted(goalName, slotName) {
        return this._isEntityFulfilled(goalName, slotName);
    }

    // TODO: should be called isSlotCompleted (see above)
    _isEntityFulfilled(goalName, slotName) {
        let slot = this.slots[goalName].filter(slot => slot.name == slotName);
        if (slot.length > 0) {
            slot = slot[0];

            if (slot.instance) {
                return slot.instance.isCompleted();
            }
        }

        return false;
    }

    getSlot(groupName, slotName) {
        let slots = this.slots[groupName].filter(s => s.name == slotName);

        if (slots.length == 0) {
            return false;
        }

        return slots[0];
    }

    // TODO: almost the same as above...
    getSlotValue(groupName, slotName) {
        let slot = this.getSlot(groupName, slotName);

		if (!slot) {
			return false;
		}

		return slot.instance.value;
    }

	_getGoalSlots(goal) {
		let slots = [];
		goal.slots.forEach((slotData) => {
			slots.push(_.merge(slotData, { instance: SlotFactory.create(slotData.type, slotData) }));
		});
		return slots;
	}

    _isGoalCompleted(goalName) {
        let pendingSlots = _.filter(this.slots[goalName], slot => {
            return !slot.instance.isCompleted();
        });

        return pendingSlots.length == 0;
    }

    _fillGoalSlots(utteranceDescriptor) {
        let entities = utteranceDescriptor.getEntities(),
            currentGoalSlots = this.slots[this._currentGoal.name],
            filledSlots = [];

        if (currentGoalSlots) {
            _.each(currentGoalSlots, (slotValue, slotKey) => {
                _.each(entities, (entityValue, entityKey) => {
                    if (entityKey == slotKey) {
                        currentGoalSlots[slotKey] = entityValue[0];
                        filledSlots.push({ key: slotKey, value: entityValue[0], goal: this._currentGoal });
                    }
                });
            });
        }

        return filledSlots;
    }

    enterStateEnd() {
		// TODO: ?
    }

    pushGoalState(stateName) {
        if (!this._currentGoal.states[stateName]) {
            BotLog.logWarning(`Transition to state ${stateName} is not allowed as state does not exist in this context.`);
        }

        // given a state, decide which actions should be queued by checking their guard conditions
        let consumableActions = BBDEngine.matchStateActions(this, this._currentGoal.states[stateName] || []);

        _.each(consumableActions, ca => {
            _.each(ca.actions, action => {
                this._actionsQueue.push(action);
            });
        });
    }

    _getNextGoal() {
        let activePendingGoals = _.filter(this._allGoals, goal => goal.active && !goal.solved),
            sortedSet = _.sortBy(activePendingGoals, ["priority"]);

        if (activePendingGoals.length > 0) {
            let mostUrgentGoal = _.last(sortedSet);

            if (mostUrgentGoal.abstract) {
                // choose from the goal's children
                let unresolvedChildren = _.filter(this._allGoals, goal => (goal.parent == mostUrgentGoal.name) && !goal.solved);
                unresolvedChildren = _.sortBy(unresolvedChildren, ["priority"]);

                return _.last(unresolvedChildren);
            } else {
                return mostUrgentGoal;
            }
        } else {
            // there are no active pending goals => all goals fulfilled?
            // if it has a default goal => select it
            if (this._defaultGoal) {
                BBDEngine.executeAction("std.reset-goal," + this._defaultGoal.name, this, false);
                return this._defaultGoal;
            }
        }

        return false;
    }

    getFact(factName) {
		// TODO: ?
    }

    getCurrentTurn() {
    	return this._currentTurn;
	}

    switchTurn() {
    	this.tutor.resetStepScore();
    	this._currentTurn = this._currentTurn == this.TURN_USER ? this.TURN_COM : this.TURN_USER;
    	this._BotCom.switchTurn(this._currentTurn);
	}

    log(value) {
        BotLog.log(value);
    }

    logError(value) {
        BotLog.logError(value);
    }

	get analyzer() {
		return new Analyzer(this, this.rsBrains);
	}

	get currentGoal() {
		return this._currentGoal;
	}

	get sessionId() {
		return this._sessionId;
	}

	setMemory(path, value) {
		this.MEMORY.memset(path, value);
	}

	deleteMemory(path) {
        this.MEMORY.memdel(path);
    }

	getMemory(path, defaultValue = false) {
		return this.MEMORY.memget(path, defaultValue);
	}

	handleSmallTalk() {
		let utteranceDescriptor = _.last(this._utteranceStack);

		if (utteranceDescriptor.hasIntent("ask-name")) {
			 // its small talk!
			this._actionsQueue = [];
			BBDEngine.executeAction("std.speak-custom,My name is Amanda!", this, false);
		}
	}
}