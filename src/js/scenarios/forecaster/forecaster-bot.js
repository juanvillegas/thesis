import Chatterbot from "../../chatterbot/chatterbot";
import WebBotCom from "../../chatterbot/components/web-bot-com";

export default class ForecasterBot extends Chatterbot {

    constructor(config = {}, scenarioDescriptor = {}) {
        let botCom = new WebBotCom();

        scenarioDescriptor = {
            "goals": [
                {
                    "name": "introduce",
                    "active": false,
                    "priority": 100,
                    "states": {}
                },
                {
                    "name": "weather-inquiry",
                    "abstract": true,
                    "active": false,
                    "priority": 90
                },
                {
                    "name": "set-location",
                    "parent": "weather-inquiry",
                    "active": true,
                    "priority": 100,
                    "slots": [{
                        "name": "location",
                        "type": "location",
                        "entities": [],
                        "intents": [""]
                    }],
                    "states": {}
                },
                {
                    "name": "set-date",
                    "parent": "weather-inquiry",
                    "active": true,
                    "priority": 100,
                    "slots": [{
                        "name": "target-date",
                        "type": "date-interval",
                        "rules": ["future", "minAccuracy,month"],
                        "intents": ["any"],
                        "entities": ["datetime", "interval"]
                    }],
                    "states": {}
                },
                {
                    "name": "clothing-inquiry",
                    "active": false,
                    "priority": 80,
                    "slots": [{

                    }],
                    "states": {}
                }
            ],
            "facts": [
                { "text": "bot.firstname", "value": "Juan" },
                { "text": "bot.lastname", "value": "Villegas" },
                { "text": "bot.age", "value": 32 },
                { "text": "bot.occupation", "value": "Climate Assistant" },
                { "text": "location.city", "value": "Capital Federal" },
                { "text": "location.province", "value": "Capital Federal" },
                { "text": "location.country", "value": "Argentina" }
            ],
            "utterances": [
                {
                    "id": "",
                    "values": []
                }
            ]
        };

        super(botCom, config, scenarioDescriptor);
    }

}