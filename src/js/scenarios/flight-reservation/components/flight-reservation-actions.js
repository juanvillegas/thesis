import _ from "lodash";
import StandardActions from "../../../chatterbot/components/standard-actions";
import FlightSlot from "../model/FlightSlot";
import FlightGenerator from "./flight-generator";

export default class FlightReservationActions {

    static checkFlightStatus(chatterbot, previousResponse) {
        let statuses = ["delayed", "on time", "not available"];

        let flightCode = chatterbot.getSlotValue("check-flight-status", "flight-code");

        let flightStatus = _.get(chatterbot.getMemory("facts.flight-status." + flightCode));
        if (!flightStatus) {
            flightStatus = statuses[_.random(statuses.length - 1)];
            chatterbot.setMemory("facts.flight-status." + flightCode, flightStatus);
        }

        return flightStatus;
    }

	/**
	 * Looks in the chatterbot goals and entities related to searching a flight
	 * Connects the bot with the data repository and fetches flights
	 *
	 * @param chatterbot
	 * @param previousResponse
	 * @param optionsContextName
	 */
    static searchFlights(chatterbot, previousResponse, optionsContextName) {
		let data = {
			departure_location: chatterbot.getSlotValue("set-origin", "departure_location"),
			destination_location: chatterbot.getSlotValue("set-destination", "destination_location"),
			seat_type: chatterbot.getSlotValue("set-flight-preferences", "seat_type"),
			departure_datetime: chatterbot.getSlotValue("set-departure-datetime", "departure-date")
		};

		let flights = FlightGenerator.generateFlights(data);

        _.each(flights, (v, k) => {
            v.toString = function() {
                return this.code + " of " + this.airline + " with a total cost of $" + this.cost;
            }
        });

        let ctx = chatterbot.MEMORY.getContext(optionsContextName);
        ctx.setOptions(flights);
    }

    /**
     * Returns true if the flight pool hasn't been created, or if it is Empty.
     * @param chatterbot
     * @param previousResponse
     * @returns {boolean}
     */
    static isFlightsPoolEmpty(chatterbot, previousResponse) {
        let flightPool = chatterbot.getMemory("flight_stack");
        if (Array.isArray(flightPool)) {
            return flightPool.length == 0;
        }

        return true;
    }

    /**
     * Shows the flight at the top of the flight stack, if it exists.
     * @param chatterbot
     * @param previousResponse
     */
    static showFlight(chatterbot, previousResponse) {
        if (FlightReservationActions.isFlightsPoolEmpty(chatterbot, previousResponse)) {
            return;
        }

        let flightPool = chatterbot.getMemory("flight_stack");
        let aFlight = flightPool.shift();

        chatterbot.setMemory("flight_active", aFlight);

        // TODO: the chatbot should allow some sort of "hooking", so that the rest of the code can inyect actions into it,
        // like displaying a flight visually, reproducing some audio, etc.

        chatterbot.log(`Flight ${aFlight.code} of ${aFlight.airline} airlines. Destination: ${aFlight.destination} leaving from ${aFlight.origin}. Total cost: ${aFlight.cost}`);
    }

    static fillFlight(chatterbot, previousResponse, slotGroup, slotName) {
        let slot = _.get(chatterbot.slots, slotGroup, []).filter(s => s.name == slotName);

        if (slot.length == 0) {
            return;
        }

        slot = slot[0];

        let selectedFlight;

        // the user has selected one of the options in the context
        let utteranceDescriptor = _.last(chatterbot._utteranceStack);
        selectedFlight = utteranceDescriptor.getEntity("selection");

        if (selectedFlight !== false) {
            slot.instance = new FlightSlot("flight", selectedFlight.value);
        }
    }

    /**
     * Copies the fact location.city to a LocationEntity and fills the corresponding <goal-entity>
     * @param chatterbot
     * @param previousResponse
     */
    static setDepartSameAirport(chatterbot, previousResponse) {
        let airportName = chatterbot.MEMORY.getFact("location.city", false);

        StandardActions.setSlot(chatterbot, previousResponse, "set-origin", "departure_location", airportName);
    }

}