var _ = require('lodash');

export default class FlightGenerator {


	/**
	 * Data is expected to be an object with:
	 * + departure_location
	 * + destination_location
	 * + departure_datetime = { from: moment , to: moment }
	 *
	 * @param data
	 * @returns {*}
	 */
	static generateFlights(data) {
		let instance = new FlightGenerator();
		instance.setData(data);
		return instance.getFlights();
	}

	constructor() {
		this.airlines = [{ name: "Gol", code: "GO" }, { name: "Aerolíneas Argentinas", code: "AA" }, { name: "Lan", code: "LA" }];
		this.seatTypes = ["economy", "first"];
		// graph of valid destinations
		this.routes = {
			"Viedma": [{ to: "Buenos Aires", cost: 4 }],
			"Bahia Blanca": [{ to: "Buenos Aires", cost: 2 }],
			"Buenos Aires": [
				{ to: "Posadas", cost: 2}, { to: "Cordoba", cost: 1 }, { to: "Santiago de Chile", cost: 2 },
				{ to: "Viedma", cost: 4 }, { to: "Bahia Blanca", cost: 1 }, { to: "Florianopolis", cost: 4 }],
			"Florianopolis": [
				{ to: "Buenos Aires", cost: 4 }, { to: "Santiago de Chile", cost: 4 }, { to: "Posadas", cost: 2 }],
			"Posadas": [{ to: "Florianopolis", cost: 2 }, { to: "Buenos Aires", cost: 2 }],
			"Cordoba": [{ to: "Buenos Aires", cost: 1 }, { to: "Santiago de Chile", cost: 2 }],
			"Santiago de Chile": [{ to: "Buenos Aires", cost: 3 }, { to: "Florianopolis", cost: 4 }, { to: "Cordoba", cost: 2} ]
		};
	}

	getAirline() {
		return this.airlines[_.random(0,2)];
	}

	makeFlight() {
		let airline = this.getAirline();
		let flight = {
			airline_id: airline.code,
			airline: airline.name,
			seat_type: this.getSeatType(),
			code: FlightGenerator.generateRandomCode(airline.code),
			departure_location: this._data.departure_location,
			destination_location: this._data.destination_location
		};

		let dateDifference = this._data.departure_datetime.from.diff(this._data.departure_datetime.to, "minutes");
		flight.departure_date = this._data.departure_datetime.from.clone().add(_.random(0, dateDifference), "minutes");

		flight.cost = this.getCost(flight.airline_id, flight.departure_location, flight.destination_location, flight.seat_type);

		return flight;
	}

	getCost(airlineId, departure, destination, seatType) {
		let calculatedCost = 200;

		let route = this.getRoutePart(departure, destination);
		calculatedCost *= route.cost;

		if (airlineId == "AA") {
			calculatedCost *= 1.15;
		}

		if (airlineId == "LA") {
			calculatedCost *= 1.25;
		}

		if (airlineId == "GO") {
			calculatedCost *= 0.85;
		}

		if (seatType == "first") {
			calculatedCost *= 3;
		}

		return Math.round(calculatedCost);
	}

	getRoutePart(dep, dest) {
		if (!this.routes[dep]) {
			return false;
		}

		let t = this.routes[dep].filter(r => r.to != dest);
		if (t.length > 0) {
			return t[0];
		}

		return false;
	}

	isValidRoute(dep, dest) {
		return this.getRoutePart(dep, dest) !== false;
	}

	setData(data) {
		this._data = data;
	}

	getFlights() {
		if (!this.hasFlights()) {
			return [];
		}

		let flights = [];

		for (let i = 0; i < _.random(5, 10); i++) {
			flights.push(this.makeFlight());
		}
		return flights;
	}

	/**
	 * 1 chance out of 10 of not having results.
	 * @returns {boolean}
	 */
	hasFlights() {
		return _.random(0,10) > 0;
	}

	static generateRandomCode(prefix) {
		return prefix + _.random(100, 999);
	}

	getSeatType() {
		return this.seatTypes[_.random(0, this.seatTypes.length - 1)];
	}

}