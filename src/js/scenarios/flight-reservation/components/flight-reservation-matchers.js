import _ from "lodash";

export default class FlightReservationMatchers {


    /**
     * Returns true if the flight pool hasn't been created, or if it is Empty.
     * @param chatterbot
     * @param previousResponse
     * @returns {boolean}
     */
    static isFlightsPoolEmpty(chatterbot, previousResponse) {
        let flightPool = chatterbot.getMemory("flight_stack");
        if (Array.isArray(flightPool)) {
            return flightPool.length == 0;
        }

        return true;
    }

}