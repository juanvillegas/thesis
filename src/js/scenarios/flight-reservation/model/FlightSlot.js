import { Slot } from "../../../chatterbot/model/Slots";

export default class FlightSlot extends Slot {

    constructor(value, data = {}) {
        super(value);
        this._code = data.code || false;
    }

    get code() {
        return this._code;
    }


    toString() {
        return "flight code is: " + this.code;
    }

    reset() {
        super.reset();
        this._code = false;
    }
}
