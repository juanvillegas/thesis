export default [
	{
		"facts": [
			{ "text": "bot.firstname", "value": "Amanda" },
			{ "text": "bot.lastname", "value": "Clarke" },
			{ "text": "bot.age", "value": 32 },
			{ "text": "bot.occupation", "value": "Sales Assistant at Ezeiza Airport" },
			{ "text": "location.city", "value": "Buenos Aires" },
			{ "text": "location.province", "value": "Buenos Aires" },
			{ "text": "location.country", "value": "Argentina" },
			{ "text": "flight-status.AA123", "value": "Delayed" },
			{ "text": "flight-status.AA300", "value": "Delayed" },
			{ "text": "flight-status.AA320", "value": "Delayed" },
			{ "text": "flight-status.AA476", "value": "On time" },
			{ "text": "flight-status.GO200", "value": "Unknown" }
		],
		"restrictions": [
			{
				"goal": "check-flight-status",
				"slot": "flight-code",
				"value": "AA476",
				"type": "alpha-equality",
                "nice": "Check the flight status for flight AA476"
			},
			{
				"goal": "set-origin",
				"slot": "departure_location",
				"value": ["Bahia Blanca", "Viedma"],
				"type": "inset",
                "nice": "Book a flight with origin Viedma or Bahia Blanca"
			},
			{
				"goal": "set-destination",
				"slot": "destination_location",
				"value": "Buenos Aires",
				"type": "equality",
                "nice": "Book a flight with destination Buenos Aires"
			},
			{
				// TODO: find a way to calculate the interval value
				"goal": "set-departure-datetime",
				"slot": "departure-date",
                // ISO8601: YYYY-MM-DDThh:mm-03:00
				"value": { "from": "2017-11-01T00:00-03:00", "to": "2017-11-031T00:00-03:00" },
				"rules": ["accuracy:month"],
				"type": "date-interval",
                "nice": "Find a flight leaving in November (between November 1st and November 31st)"
			},
			{
				"goal": "after-purchase",
				"slot": "has-luggage",
				"value": false,
				"type": "boolean",
                "nice": "When asked, indicate to the sales assistant that you don't have any luggage to dispatch"
			},
			{
				"goal": "print-boarding-pass",
				"slot": "username",
				"value": "Frank Stevens",
				"type": "loose-equality",
                "nice": "Make sure your boarding pass says your name, 'Frank Stevens'"
			},
			{
				"goal": "print-boarding-pass",
				"slot": "id",
				"value": "33666032",
				"type": "equality",
                "nice": "Your boarding pass should state your national ID, '33666032'"
			}
		]
	},
    {
        "facts": [
            { "text": "bot.firstname", "value": "Amanda" },
            { "text": "bot.lastname", "value": "Clarke" },
            { "text": "bot.age", "value": 32 },
            { "text": "bot.occupation", "value": "Sales Assistant at Ezeiza Airport" },
            { "text": "location.city", "value": "Buenos Aires" },
            { "text": "location.province", "value": "Buenos Aires" },
            { "text": "location.country", "value": "Argentina" },
            { "text": "flight-status.AA123", "value": "Delayed" },
            { "text": "flight-status.AA300", "value": "Delayed" },
            { "text": "flight-status.AA320", "value": "Delayed" },
            { "text": "flight-status.AA476", "value": "On time" },
            { "text": "flight-status.GO200", "value": "Unknown" }
        ],
        "restrictions": [
            {
                "goal": "set-origin",
                "slot": "departure_location",
                "value": "Buenos Aires",
                "type": "loose-equality",
                "nice": "Book a flight with origin Buenos Aires"
            },
            {
                "goal": "set-destination",
                "slot": "destination_location",
                "value": "Posadas",
                "type": "loose-equality",
                "nice": "Book a flight with destination Posadas"
            },
            {
                // TODO: find a way to calculate the interval value
                "goal": "set-departure-datetime",
                "slot": "departure-date",
                // ISO8601: YYYY-MM-DDThh:mm-03:00
                "value": "tomorrow",
                "rules": ["accuracy:day","restrictions:night"],
                "type": "date-interval",
                "nice": "Find a flight leaving tomorrow at night"
            },
            {
                "goal": "check-luggage",
                "slot": "has-luggage",
                "value": true,
                "type": "boolean-equality",
                "nice": "When asked, indicate to the sales assistant that you have luggage to dispatch"
            },
            {
                "goal": "print-boarding-pass",
                "slot": "username",
                "value": "Frank Stevens",
                "type": "loose-equality",
                "nice": "Make sure your boarding pass says your name, 'Frank Stevens'"
            },
            {
                "goal": "print-boarding-pass",
                "slot": "id",
                "value": "33666032",
                "type": "equality",
                "nice": "Your boarding pass should say your national ID, '33666032'"
            }
        ]
    }
];