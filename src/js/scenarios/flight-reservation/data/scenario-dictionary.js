export default {
    "ticket": {
        "index": "ticket",
        "value": "Ticket",
        "definition": "a piece of paper or card that gives the holder a certain right, especially to enter a place, travel by public transport, or participate in an event.",
        "plural": "Tickets",
        "type": "noun",
        "synonyms": [],
        "examples": [
            "The lottery tickets were not claimed",
            "We can use the machines at the airport to print our tickets"
        ]
    },
    "flight": {
        "index": "flight",
        "value": "Flight",
        "definition": "a journey in a plane",
        "plural": "Flights",
        "type": "noun",
        "synonyms": [],
        "examples": [
            "My flight is delayed",
            "My return flight is on Monday"
        ]
    },
    "itinerary": {
        "index": "itinerary",
        "value": "Itinerary",
        "definition": "a detailed plan or route of a journey",
        "plural": "Itineraries",
        "type": "noun",
        "synonyms": ["planned route", "route", "path"],
        "examples": [
            "The tour operator will arrange transport and plan your itinerary.",
            "I spent weeks planning my itinerary"
        ]
    },
    "flight code": {
        "index": "flight code",
        "value": "Flight Code",
        "definition": "it's the number identifying your flight",
        "plural": "Flight codes",
        "type": "noun",
        "synonyms": ["flight number"],
        "examples": [
            "You can check the status of the flight by looking your flight code in the airport's flight board",
            "Is this the baggage lane for flight code $var(flightCode)"
        ]
    },
	"baggage": {
	    "index": "baggage",
		"value": "Baggage",
		"definition": "suitcases and bags containing personal belongings packed for travelling; luggage.",
		"plural": "baggages",
		"type": "noun",
		"synonyms": ["luggage"],
		"examples": [
			"we collected our baggage before clearing customs",
			"can you please put your baggage in the tape?"
		]
	},
	"luggage": {
        "index": "luggage",
		"value": "Luggage",
		"definition": "suitcases or other bags in which to pack personal belongings for travelling",
		"plural": false,
		"type": "noun",
		"synonyms": ["baggage"],
		"examples": [
			"after landing we waited a long time to pick up our luggage",
			"where is the luggage deposit in this airport?"
		]
	},
	"boarding pass": {
        "index": "boarding pass",
		"value": "Boarding Pass",
		"definition": "a pass for boarding an aircraft, given to passengers when checking in.",
		"plural": "boarding passes",
		"type": "noun",
		"synonyms": ["boarding card", "boarding ticket"],
		"examples": [
			"the lady from the airline gave us our boarding passes",
			"if you lose your boarding pass you'll not be able to board the plane"
		]
	}
}