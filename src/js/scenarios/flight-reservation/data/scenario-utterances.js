export default [
    {
        "id": "invalid-location-provided",
        "values": [
            { "text": "I'm sorry, but that doesn't seem to be a valid option. Our connections include Cordoba, Posadas, Santiago de Chile, Buenos Aires, Viedma, Bahia Blanca and the beautiful Florianopolis." }
        ]
    },
    {
        "id": "present-boarding-pass",
        "suggestions": [
            "$action(thank)"
        ],
        "values": [
            { "text": "Ok! Here is your boarding pass! Enjoy your flight!" },
            { "text": "Perfect. Here is your boarding pass. Thanks for flying with our company!" },
            { "text": "All right, here is your boarding ticket. Please continue to the gates when requested. Thanks!" },
        ]
    },
    {
        "id": "ask-passenger-name",
        "suggestions": [
            "My name is $var(name)"
        ],
        "values": [
            {
                "text": "Can you please tell me your name?",
                "type": "direct-query",
                "brain": "sayName"
            }
        ]
    },
    {
        "id": "ask-passenger-id",
        "values": [
            {
                "text": "Can you please tell me your ID?",
                "type": "direct-query",
                "brain": "sayIdentification"
            }
        ]
    },
    {
        "id": "print-boarding-pass",
        "values": [
            { "text": "Ok, here is your boarding pass. Use it to access the boarding area, and remember to be there on time!" },
            { "text": "Ok, this is your boarding pass. Please pay attention to the flight charts for updates about your flight." },
            { "text": "Perfect! Heere is your boarding pass." }
        ]
    },
    {
        "id": "ask-luggage",
        "suggestions": [
            "Yes, this suitcase and this carry-on bag.", // TODO: NLU for this?
            "I $set(will,want to,would like to) dispatch these bags",
            "I have these bags",
            "Here are my bags",
            "This is my luggage",
            "I do not have any $set(luggage,bags)",
        ],
        "values": [
            { "text": "Do you have any luggage to dispatch?" },
            { "text": "Is there any luggage to dispatch?" },
            { "text": "Will you dispatch any bags?" }
        ]
    },
    {
        "id": "confirm-luggage",
        "values": [
            { "text": "Ok. Can you please put your bags in the tape, please?" }
        ]
    },
    {
        "id": "ask-origin",
        "suggestions": [
            "I $set(want,would like) to depart from $var(location)",
            "From $var(location)"
        ],
        "values": [
            { "text": "Where do you want to depart from?" },
            { "text": "Where would you like to depart from?" }
        ]
    },
    {
        "id": "ask-same-origin",
        "values": [
            {
                "text": "Do you want to depart from this Airport?",
                "type": "binary-query",
                "true-intent": "depart-same-airport",
                "false-intent": "depart-different-airport"
            }
        ]
    },
    {
        "id": "flight-confirm-query",
        "values": [
            {
                "text": "Do you want this flight?",
                "type": "binary-query",
                "true-intent": "select-option",
                "subject": "option",
                "true-value": "current-option"
            },
            {
                "text": "Is this flight ok for you?",
                "type": "binary-query",
                "true-intent": "select-option",
                "subject": "option",
                "true-value": "current-option"
            }
        ]
    },
    {
        "id": "inform-itinerary-complete",
        "values": [
            { "text": "Great! Now I have all the details I need to get you a flight." }
        ]
    },
    {
        "id": "inform-flight-search",
        "values": [
            { "text": "Wait a moment, I'm looking for the best deals for you..." }
        ]
    },
    {
        "id": "validation.departure-date.future",
        "values": [
            { "text": "The departure date needs to be in the future!" },
            { "text": "Sorry, but I'm afraid you can't depart in the past!" },
            { "text": "You can only buy tickets for planes that haven't left yet." }
        ]
    },
    {
        "id": "no-more-flights",
        "values": [
            { "text": "I'm afraid that's all the flights I could find given your requirements." },
            { "text": "Sorry, but there are no more flights available!" }
        ]
    },
    {
        "id": "new-itinerary",
        "values": [
            {
                "text": "Do you want to try with a new itinerary?",
                "type": "binary-query",
                "true-intent": "accept",
                "false-intent": "refuse"
            }
        ]
    },
    {
        "id": "date-not-precise",
        "values": [
            { "text": "I'm afraid you'll have to narrow your timeframe a little bit." },
            { "text": "Sorry, but I'm going to need you to be more precise about that!" }
        ]
    },
    {
        "id": "ask-flight-type",
        "suggestions": [
            "I $set(really,honestly,) don't have a preference",
            "I want a direct flight whenever possible"
        ],
        "values": [
            {
                "text": "Do you prefer a direct flight or want to explore all options?",
                "suggestions": [
                    "I want to consider all options",
                    "Let's explore all options"
                ],
                "type": "options-query",
                "subject": "flight-type",
                "if-true": "flight-preferences,flight-type,any"
            },
            {
                "text": "Do you have any preferences about your flight layovers?",
                "type": "options-query",
                "subject": "flight-type",
                "if-true": "flight-preferences,flight-type,any"
            }
        ]
    },
    {
        "id": "ask-seat-type",
        "values": [
            {
                "text": "Do you need first class seats?",
                "type": "binary-query",
                "subject": "seat-type",
                "true-value": "first-class-seats",
                "false-value": "any-seats"
            },
            {
                "text": "Would you like to upgrade your ticket to first class?",
                "type": "binary-query",
                "subject": "seat-type",
                "true-value": "first-class-seats",
                "false-value": "any-seats"
            }
        ]
    },
    {
        "id": "ask-flight-code",
        "suggestions": [
            "Sure, my flight code is $var(flightCode)"
        ],
        "values": [
            { "text": "Can you please tell me the code of your flight?" },
            { "text": "Tell me your flight code." },
            { "text": "What's your flight code?" },
            { "text": "Do you have your flight code at hand?" }
        ]
    },
    {
        "id": "inform-flight-status",
        "values": [
            {
                "text": "The status for flight $1 is $2",
                "replacers": [
                    "$slot|check-flight-status,flight-code",
                    "$fact|flight-status.$1"
                ]
            },
            {
                "text": "Flight $1 is $2",
                "replacers": [
                    "$slot|check-flight-status,flight-code",
                    "$fact|flight-status.$1"
                ]
            }
        ]
    },
    {
        "id": "ask-departure-time",
        "values": [
            { "text": "What time would you like to depart?", "topics": [{ "text": "departure-time.ask" }] },
            { "text": "Ok so, regarding departure, what time would you like to travel?", "topics": [{ "text": "departure-time.ask" }] },
            { "text": "Ok so, regarding departure, what time would you like to travel?", "topics": [{ "text": "departure-time.ask" }] },
        ]
    },
    {
        "id": "ask-departure-date",
        "values": [
            { "text": "When would you like to leave?", "topics": [{ "text": "departure-date.ask" }] }
        ]
    },
    {
        "id": "confirm-departure-date",
        "values": [
            { "text": "Your flight is departing $1", "replacers": ["$slot|set-departure-datetime,date"] }
        ]
    },
    {
        "id": "confirm-departure-time",
        "values": [
            { "text": "Your flight is departing at $1", "replacers": ["$slot|set-departure-datetime,time"] }
        ]
    },
    {
        "id": "ask-destination",
        "suggestions": [
            "I'm $set(travelling,flying) to $var(destination)" ,
            "I would like to $set(fly,go,travel) to $var(destination)",
            "My $set(final) destination is $var(destination)"
        ],
        "values": [
            { "text": "What's your final destination?" },
            { "text": "Where are you flying to today?" },
            {
                "text": "Where would you like to fly to?",
                "topics": [
                    { "text": "destination.ask" }, { "text": "query-slot.set-destination.destination_location" }
                ]
            },
            { "text": "Where are you travelling to?", "topic": [{ "text": "goal.set_destination" }, { "text": "query.set_destination" }] },
            { "text": "Where do you want to go to?", "topics": [{ "text": "goal.set_destination" }, { "text": "query.set_destination" }] },
            { "text": "What's your destination?", "topics": [{ "text": "goal.set_destination" }, { "text": "query.set_destination" }] }
        ]
    },
    {
        "id": "ground-departure-location",
        "values": [
            { "text": "Ok, setting departure location to $1", "replacers": ["$slot|set-origin,departure_location"] }
        ]
    },
    {
        "id": "ground-destination",
        "values": [
            { "text": "Ok, destination set to $1", "replacers": ["$slot|set-destination,destination_location"] },
            { "text": "$1 is an excellent choice!", "replacers": ["$slot|set-destination,destination_location"] }
        ]
    },
    {
        "id": "set-departure-datetime",
        "suggetions": [
            { "text": "I would like to travel in the morning" },
            { "text": "I'm okay travelling any day next week" }
        ],
        "values": [
            { "text": "What is your travel date?", "topics": [{ "text": "query.set_departure_date" }] },
            { "text": "What date will you be travelling?", "topics": [{ "text": "query.set_departure_date" }] },
            { "text": "When do you want to travel?", "topics": [{ "text": "query.set_departure_date" }] },
            { "text": "When would you like to depart?", "topics": [{ "text": "query.set_departure_date" }] }
        ]
    },
    {
        "id": "confirm-set",
        "values": [
            { "text": "Fantastic! Hold on a minute please...", "topics": [] },
            { "text": "That's great, let's see...", "topics": [] }
        ]
    },
    {
        "id": "confused",
        "values": [
            { "text": "Umm, I'm unsure what you meant to say. Let's try again." },
            { "text": "Sorry, I didn't get that!" }
        ]
    },
    {
        "id": "introduce-ask-next",
        "values": [
            { "text": "Ok, how can I help you?" },
            { "text": "Ok, what can I do for you?" }
        ]
    },
    {
        "id": "undetected-flight-code",
        "values": [
            { "text": "Sorry, but I couldn't detect tour flight code. Let's try again." },
            { "text": "Did you spell your flight code correctly? Try again please." }
        ]
    },
    {
        "id": "invalid-flight-code",
        "values": [
            { "text": "The flight code you entered seems invalid. For example, valid flight codes contain 2 or 3 letters followeb by 3 numbers." }
        ]
    },
    {
        "id": "introduce-set",
        "suggestions": [
            "What is the status of flight $var(flightCode)?",
            "Can you tell me the status of my flight?",
            "Has $var(flightCode) landed?",
            "I $set(need,want) to check the status of my flight",
            "*Can you tell me the status of flight $var(flightCode)",
            "*I would like to buy a ticket to $var(location)",
            "I want to buy a ticket",
            "Sell me a ticket",
            "I $set(need,want) a flight to $var(location)"
        ],
        "values": [
            {
                "text": "Can I help you with something else?",
                "type": "binary-query",
                "true-intent": "accept",
                "false-intent": "refuse",
                "conditions": ["!std.is-first-utterance"]
            },
            {
                "text": "Is there anything else I can help you with?",
                "conditions": ["!std.is-first-utterance"]
            },
            {
                "text": "Hello! My name is $1. How may I help you today?",
                "replacers": ["$fact|bot.firstname"],
                "topics": [{ "text": "bot.firstname" }]
            },
            {
                "text": "Hello and welcome to the Airport Reservation System! My name is $1. What can I do for you?",
                "replacers": ["$fact|bot.firstname"],
                "topics": [{ "text": "bot.firstname" }]
            },
        ]
    }
];