let scenarioVariations = require("./scenario-variations").default;
let scenarioDictionary = require("./scenario-dictionary").default;
let scenarioUtterances = require("./scenario-utterances").default;

export default {
	"title": "Buying a ticket at the Airport",
	"roles": [],
	"description": "TODO: describe it",
    "goals": [
        {
            "name": "introduce",
			"nice": "Express yourself",
			"description": "",
            "default": true,
            "brain": "introductions",
			"displayable": true,
            "active": true,
            "priority": 100,
            "states": {
                "init": [
                    {
                        "actions": [
                            "std.speak,introduce-set",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.if-then,(std.compare-intent,accept),(std.go-to-state,ask-next-action)",
                            "std.if-then,(std.compare-intent,refuse),(std.end)",
                            "std.go-to-state,analyze-reply"
                        ]
                    }
                ],
                "ask-next-action": [{
                    "actions": [
                        "std.speak,introduce-ask-next",
						"std.listen",
						"std.process-utterance,flight-reservation-analyzer",
                        "std.go-to-state,analyze-reply"
                    ]
                }],
                "analyze-reply": [{
                    "actions": [
                        "std.if-then,(std.compare-intent,[flight-status,check-flight-status]),(std.go-to-state,flight-status)",
                        "std.if-then,(std.compare-intent,book-flights),(std.go-to-state,book-flight)",
                        "std.speak,confused"
                    ]
                }],
                "flight-status": [{
                    "actions": [
                        "std.tutor-finish-step",
                        "std.if-then,(std.has-validation-errors,flight-code),(std.go-to-state,invalid-flight-code)",
                        "std.solve-goal,introduce",
                        "std.reset-goal,check-flight-status",
                        "std.fill-slot,check-flight-status,flight-code",
                        "std.activate-goal,check-flight-status"
                    ]
                }],
                "invalid-flight-code": [
                    {
                        "actions": [
                            "std.speak,invalid-flight-code",
                            "std.go-to-state,init"
                        ]
                    }
                ],
                "book-flight": [{
                    "actions": [
                        "std.tutor-finish-step",
                        // TODO: update slots: dest_location, time, whatever, if provided in a utterance like:
                        "std.fill-slot,set-departure-datetime,departure-date",
                        "std.fill-slot,set-destination,destination_location",
                        "std.fill-slot,set-origin,departure_location",
                        // "i want to buy a ticket from california to san francisco for next tuesday"..
                        // "std.update-slot,check-flight-status,flight-code",
                        "std.solve-goal,introduce",
                        "std.activate-goal,set-itinerary"
                    ]
                }]
            }
        },
        {
            "name": "check-flight-status",
			"nice": "Check flight status",
			"description": "",
            "active": false,
			"displayable": true,
            "priority": 70,
			"brain": "airportGenerics",
            "slots": [
                {
                    "name": "flight-code",
                    "type": "value",
                    "rules": [
                        "regex|^[a-zA-Z]{2,3}[0-9]{3}$"
                    ],
                    "intents": ["check-flight-status", "flight-status"],
                    "entities": ["flight-code", "flight-number"]
                }
            ],
            "states": {
                "init": [
                    {
                        "condition": ["std.has-entity,flight-code"],
                        "actions": [
                            "std.go-to-state,check-flight-status"
                        ]
                    },
                    {
                        "condition": ["std.needs-entity,flight-code"],
                        "actions": [
                            "std.go-to-state,get-flight-code"
                        ]
                    }
                ],
                "get-flight-code": [
                    {
                        "actions": [
                            "std.speak,ask-flight-code",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.fill-slot,check-flight-status,flight-code",
                            "std.if-then,(std.has-validation-errors,flight-code),(std.go-to-state,invalid-flight-code)",
                            "std.if-then,(std.needs-entity,flight-code),(std.go-to-state,no-flight-code)",
                            "std.go-to-state,check-flight-status"
                        ]
                    }
                ],
                "no-flight-code": [
                    {
                        "actions": [
                            "std.speak,undetected-flight-code",
                            "std.go-to-state,get-flight-code"
                        ]
                    }
                ],
                "invalid-flight-code": [
                    {
                        "actions": [
                            "std.speak,invalid-flight-code",
                            "std.go-to-state,get-flight-code"
                        ]
                    }
                ],
                "check-flight-status": [
                    {
                        "actions": [
                            "fr.check-flight-status",
                            "std.speak,inform-flight-status",
                            "std.refresh-goals,check-flight-status"
                        ]
                    }
                ]
            }
        },
        {
            "name": "set-itinerary",
            "priority": 90,
			"nice": "Itinerary",
			"description": "",
			"displayable": true,
            "abstract": true,
            "active": false,
            "onSolved": ["std.activate-goal,sell-ticket"]
        },
        {
            "name": "set-origin",
			"nice": "Set origin",
			"description": "",
            "parent": "set-itinerary",
            "priority": 90,
			"displayable": true,
            "slots": [{
                "name": "departure_location",
                "type": "location",
                "rules": ["inset|cordoba,posadas,viedma,bahia blanca,buenos aires,florianopolis,santiago de chile"],
                "entities": ["location", "departure-location"],
                "intents": ["book-flights", "flight-schedule", "set-flight-route"]
            }],
            "states": {
                "init": [
                    {
                        "condition": ["std.needs-entity,departure_location"],
                        "actions": [
                            "std.if-then,(std.dice,50),(std.go-to-state,get-location-same)",
                            "std.go-to-state,get-location"
                        ]
                    },
                    {
                        "condition": ["std.has-entity,departure_location"],
                        "actions": [
                            "std.solve-goal,set-origin"
                        ]
                    }
                ],
                "get-location": [
                    {
                        "actions": [
                            "std.speak,ask-origin",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.fill-slot,set-origin,departure_location",
                            "std.if-then,(std.has-entity,departure_location),(std.speak,ground-departure-location)",
                            "std.if-then,(std.has-validation-errors,departure_location),(std.speak,invalid-location-provided)",
                            "std.if-then,(std.needs-entity,departure_location),(std.speak,confused)",
                            "std.go-to-state,init"
                        ]
                    }
                ],
                "get-location-same": [
                    {
                        "actions": [
                            "std.speak,ask-same-origin",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.if-then,(std.compare-intent,depart-same-airport),(std.go-to-state,same-airport)",
                            "std.go-to-state,get-location"
                        ]
                    }
                ],
                "same-airport": [
                    {
                        "actions": [
                            // "std.set-slot,set-origin,departure_location,$fact|location.city", <--- TODO: desired!!
                            "fr.set-depart-same-airport",
                            "std.speak,ground-departure-location"
                        ]
                    }
                ]
            }
        },
        {
            "name": "set-destination",
            "parent": "set-itinerary",
            "priority": 90,
			"displayable": true,
            "related": [],
            "slots": [{
                "name": "destination_location", // an identifier for the slot
                "type": "location", // the type of the Slot
                "rules": ["inset|cordoba,posadas,viedma,bahia blanca,buenos aires,florianopolis,santiago de chile"],
                "entities": ["location", "destination_location"],
                "intents": ["any", "book-flights", "flight-schedule", "set-flight-route"] // restrict the intents where this Slot can be filled
            }],
            "states": {
                "init": [
                    {
                        "condition": ["std.needs-entity,destination_location"],
                        "actions": [
                            "std.go-to-state,get-destination"
                        ]
                    },
                    {
                        "condition": ["std.has-entity,destination_location"],
                        "actions": [
                            "std.solve-goal,set-destination"
                        ]
                    }
                ],
                "get-destination": [
                    {
                        "actions": [
                            "std.speak,ask-destination",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.fill-slot,set-destination,destination_location",
                            "std.if-then,(std.has-entity,destination_location),(std.speak,ground-destination)",
                            "std.if-then,(std.has-validation-errors,destination_location),(std.speak,invalid-location-provided)",
                            "std.go-to-state,init"
                        ]
                    }
                ]
            }
        },
        {
            "name": "set-flight-preferences",
            "parent": "set-itinerary",
            "priority": 60,
			"displayable": true,
            "slots": [
                {
                    "name": "flight-type", // an identifier for the slot
                    "type": "value", // the type of the Slot
                    "entities": ["flight-type"],
                    "intents": ["flight-preferences"] // restrict the intents where this Slot can be filled
                },
                {
                    "name": "seat-type",
                    "type": "value",
                    "entities": ["seat-type"],
                    "intents": ["flight-preferences"]
                },
                // one more slot for: baggage?
            ],
            "states": {
                "init": [
                    {
                        "condition": ["std.needs-entity,flight-type"],
                        "actions": [
                            "std.go-to-state,get-flight-type"
                        ]
                    },
                    {
                        "condition": ["std.needs-entity,seat-type"],
                        "actions": [
                            "std.go-to-state,get-seat-type"
                        ]
                    },
                    {
                        "condition": ["std.has-entity,[seat-type,flight-type]"],
                        "actions": [
                            "std.go-to-state,end"
                        ]
                    }
                ],
                "get-flight-type": [
                    {
                        "actions": [
                            "std.speak,ask-flight-type",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.fill-slot,set-flight-preferences,flight-type",
                            "std.go-to-state,init"
                        ]
                    }
                ],
                "get-seat-type": [
                    {
                        "actions": [
                            "std.speak,ask-seat-type",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.fill-slot,set-flight-preferences,seat-type",
                            "std.go-to-state,init"
                        ]
                    }
                ],
                "end": [
                    {
                        "actions": [
                            "std.solve-goal,set-flight-preferences"
                        ]
                    }
                ]
            }
        },
        {
            "name": "set-departure-datetime",
            "parent": "set-itinerary",
            "priority": 90,
			"displayable": true,
            "slots": [{
                "name": "departure-date",
                "type": "date-interval",
                "rules": ["future", "minAccuracy|month"],
                "intents": ["any"],
                "entities": ["datetime", "interval"]
            }],
            "states": {
                "init": [
                    {
                        "condition": ["std.needs-entity,departure-date"],
                        "actions": [
                            "std.go-to-state,ask-departure-datetime"
                        ]
                    },
                    {
                        "condition": ["std.has-entity,departure-date"],
                        "actions": [
                            "std.solve-goal,set-departure-datetime"
                        ]
                    }
                ],
                "ask-departure-datetime": [
                    {
                        "actions": [
                            "std.speak,ask-departure-date",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.fill-slot,set-departure-datetime,departure-date",
                            "std.solve-goal,set-departure-datetime"

                            // "std.if-then,!(utterance-has-entity,datetime),(std.go-to-state,...)",// <-- TODO: intent is incorrect, redirect to goal-select
                            // "std.if-then,(dt.accuracy-lte,year),[(std.speak,date-not-precise),(std.go-to-state,init)]", // TODO: maybe the accuracy could be read from the Slot instead
                            // TODO: needs to support array of actions ^^^
                        ]
                    }
                ],
                "validate": [
                    {
                        "actions": [
                            "std.update-slot,set-departure-datetime,departure-date",
                            "std.go-to-state,init"
                        ]
                    }
                ]
            }
        },
        {
            "name": "sell-ticket",
            "active": false,
            "priority": 100,
			"displayable": true,
            "slots": [
                {
                    "name": "flight",
                    "type": "FlightEntity"
                }
            ],
            "states": {
                "init": [
                    {
                        "actions": [
                            "std.speak,inform-itinerary-complete",
                            "std.speak,inform-flight-search",
                            // context type, context name, memory reference to pool of results
                            "std.open-context,select-options,select-flight-context,flight-pool",
                            "fr.search-flights,select-flight-context",
                            "std.if-then,(std.is-context-empty,select-flight-context),std.go-to-state,no-results",
                            "std.go-to-state,show-options"
                        ]
                    }
                ],
                "show-options": [
                    {
                        "condition": ["!std.is-context-empty,select-flight-context"],
                        "actions": [
                            "std.show-context-option,select-flight-context",
                            "std.speak,flight-confirm-query",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.if-then,(std.compare-intent,[select-option]),(std.go-to-state,solve)",
                            "std.go-to-state,show-options"
                        ]
                    },
                    {
                        "conditions": ["std.is-context-empty,select-flight-context"],
                        "actions": [
                            "std.speak,no-more-flights",
                            "std.go-to-state,no-results"
                        ]
                    }
                ],
                "solve": [
                    {
                        // the user has accepted a flight
                        "actions": [
                            "fr.fill-flight,sell-ticket,flight",
                            "std.solve-goal,sell-ticket",
                            // "fr.display-flight,sell-ticket,flight", // TODO:
                            "std.close-context,select-flight-context",
                            "std.activate-goal,after-purchase"
                        ]
                    }
                ],
                "no-results": [
                    {
                        "actions": [
                            "std.speak,new-itinerary",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.if-then,(std.compare-intent,accept),(std.reset-goal,set-itinerary)",
                            "std.if-then,(std.compare-intent,accept),(std.deactivate-goal,sell-ticket)",
                            "std.if-then,(std.compare-intent,accept),(std.activate-goal,set-itinerary)"
                        ]
                    }
                ]
            }
        },
        {
            "name": "after-purchase",
            "abstract": true,
			"displayable": true,
            "active": false
        },
        {
            "name": "check-luggage",
            "parent": "after-purchase",
            "priority": 60,
			"displayable": true,
            "slots": [
                {
                    "name": "has-luggage",
                    "type": "boolean",
                    "entities": ["has-luggage"],
                    "intents": ["check-luggage"]
                }
            ],
            "states": {
                "init": [
                    {
                        "actions": [
                            "std.speak,ask-luggage",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.if-then,(std.compare-intent,[yes]),(std.go-to-state,has-luggage)",
                            "std.if-then,(std.compare-intent,[no]),(std.go-to-state,no-luggage)",
							"std.speak,confused"
                        ]
                    }
                ],
                "has-luggage": [
                    {
                        "actions": [
                            "std.speak,confirm-luggage",
                            "std.set-slot,check-luggage,has-luggage,true",
                            "std.solve-goal,check-luggage"
                        ]
                    }
                ],
                "no-luggage": [
                    {
                        "actions": [
                            "std.set-slot,check-luggage,has-luggage,false",
                            "std.solve-goal,check-luggage"
                        ]
                    }
                ]
            }
        },
        {
            "name": "print-boarding-pass",
            "parent": "after-purchase",
            "priority": 40,
			"displayable": true,
            "slots": [
                {
                    "name": "username",
                    "type": "value",
                    "entities": ["username"],
                    "intents": ["say-name"]
                },
                {
                    "name": "id",
                    "type": "value",
                    "entities": ["identification"],
                    "intents": ["say-identification"]
                }
            ],
            "states": {
                "init": [
                    {
                        "actions": [
                            "std.speak,ask-passenger-name",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.fill-slot,print-boarding-pass,username",
                            "std.speak,ask-passenger-id",
                            "std.listen",
                            "std.process-utterance,flight-reservation-analyzer",
                            "std.fill-slot,print-boarding-pass,id",
                            "std.speak,present-boarding-pass",
                            "std.solve-goal,print-boarding-pass"
                        ]
                    }
                ]
            }
        }
    ],
	"variations": scenarioVariations,
	"dictionary": scenarioDictionary,
    "utterances": scenarioUtterances
};