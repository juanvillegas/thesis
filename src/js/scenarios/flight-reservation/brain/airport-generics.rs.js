export default
`+ is flight * [delayed|on time|cancelled]
- {"intents": ["check-flight-status"], "entities": [{ "name": "flight-code", "value": "<star1>" }] }

+ has [flight] * arrived *
- {"intents": ["check-flight-status"], "entities": [{ "name": "flight-code", "value": "<star1>" }] }

+ my [final] destination is *
- {"intents": ["book-flights"], "entities": [{"name": "destination-location", "value": "<star1>" }] }

+ * [flying|going] to *
- {"intents": ["book-flights"], "entities": [{"name": "destination-location", "value": "<star1>" }] }

+ * [would like|want|like] [a ticket|to fly|to go|a flight] to *
- {"intents": ["book-flights"], "entities": [{"name": "destination-location", "value": "<star1>" }] }
`;