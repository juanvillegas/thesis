export default
`+ book me [a flight|a ticket] [please|now|right now]
- {"intents": ["book-flights"]}

+ book me [a flight|a ticket] to * [please|now|right now]
- {"intents": ["book-flights"], "entities": [{ "name": "destination_location", "value": "<star1>" }] }

+ i [urgently] [would like|need] [you] [to book|to buy|to reserve|to get] [a flight|a ticket|tickets] [for me]
- {"intents": ["book-flights"]}

+ book me [a flight|a ticket|tickets] from * to * [*]
- {"intents": ["book-flights"], "entities": [{ "name": "departure-location", "value": "<star1>" }, { "name": "destination_location", "value": "<star2>" }]}

+ i * [would like|need] [to book|to buy|to reserve|to get] [a ticket|a flight|a trip|tickets] from * to * [*]
- {"intents": ["book-flights"], "entities": [{ "name": "departure-location", "value": "<star2>" }, { "name": "destination_location", "value": "<star3>" }]}

+ can you [please] [help me book|book me] [a ticket|a flight|some tickets|some flights] [please]
- {"intents": ["book-flights"]}

+ i need help [to book|booking|buying|to buy] a [flight|ticket] to *
- {"intents": ["book-flights"], "entities":[{ "name": "destination_location", "value": "<star1>"}]}

+ i need help [booking|buying|to buy|to book] a flight
- { "intents": ["book-flights"] }

+ i want [to book|to buy] a flight to *
- {"intents": ["book-flights"], "entities":[{ "name": "destination_location", "value": "<star1>"}]}



+ [i * would like to|i * need to|i * want to] check my flight status *
- {"intents": ["check-flight-status"]}

+ [*] what is the status of flight *
- {"intents": ["check-flight-status"], "entities": [{ "name": "flight-code", "value": "<star1>" }]}

+ [can you] check another flight status *
- {"intents": ["check-flight-status"]}

+ what is my flight status 
- {"intents": ["check-flight-status"]}

+ i [would] need [you] to check my flight [status] [please]
- {"intents": ["check-flight-status"]}

+ [can you] (tell me|check) [my|the|a|some] flight status [please|now]
- {"intents": ["check-flight-status"]}

+ [i would like to|i need to|i want to|can you|can we] check the status of [a] flight *
- {"intents": ["check-flight-status"], "entities": [{ "name": "flight-code", "value": "<star1>" }]}

+ [can you|would you|why do not you] tell me the status of [a] flight *
- {"intents": ["check-flight-status"], "entities": [{ "name": "flight-code", "value": "<star1>" }]}


+ [*] my flight code is *
- {"intents": "inform-flight-code", "entities": [{"name": "flight-code", "value": "<star1>" }]}

`;
