import Chatterbot from "../../chatterbot/chatterbot";
import scenarioDescriptor from "./data/scenario-descriptor";

import SayIdCode from "../../chatterbot/brain/say-id.rs";
import SayNameCode from "../../chatterbot/brain/say-name.rs";
import IntroductionsRiveCode from "./brain/introduce.rs";
import AirportGenericsCode from "./brain/airport-generics.rs";

export default class FlightReservationBot extends Chatterbot {

    constructor(botCom, config = {}) {
        config.rsBrains = [
            { name: "sayIdentification", code: SayIdCode },
            { name: "sayName", code: SayNameCode },
            { name: "airportGenerics", code: AirportGenericsCode },
            { name: "introductions", code: IntroductionsRiveCode }
        ];

        console.log('Creating variation: ' + config.variation);

        config.scenarioVariation = config.variation;

        super(botCom, config, scenarioDescriptor);
    }

}