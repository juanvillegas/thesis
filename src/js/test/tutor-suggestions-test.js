var TutorSuggestions = require("../../../build-common/js/chatterbot/tutor/TutorSuggestions").default;
var levenshtein  = require("fast-levenshtein");

var data =
	{
		lastUtterance: {
			text: 'Hello! My name is Rick. How may I help you today?',
			topics: [ [Object] ],
			id: 'rJrGlY1DOcb',
			suggestions:
			[ 'I $set(need,want) to check the status of my flight',
				'*Can you tell me the status of flight $var(flightCode)',
				'I want to buy a ticket',
				'Sell me a ticket',
				'I $set(need,want) a flight to $var(location)' ]
		},
		input: 'Can you please tell me the status of flight abc123'
	};


var instance = new TutorSuggestions(data);

console.log(instance.getSuggestions());