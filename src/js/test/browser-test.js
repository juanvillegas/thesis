import _ from "lodash";
import $ from "jquery";
import moment from "moment";
import Bot from "../scenarios/flight-reservation/flight-reservation-bot";

let BotUtils = {
    serverLog: function (author, message, sessionId) {
        $.ajax("http://localhost:3000/sessions/" + sessionId + "/log", {
            method: "POST",
            accepts: "JSON",
            data: {
                timestamp: moment().format(),
                name: author,
                message: message
            }
        });
    }
};

window.document.addEventListener("DOMContentLoaded", function() {
    let button = document.getElementById("speechTrigger"),
        utteranceInput = document.getElementById("utteranceInput"),
        messagesList = $("#messagesList");

    let theBot = new Bot({ element: button });

    function scrollChat() {
        let height = messagesList.parent()[0].scrollHeight;
        messagesList.parent().scrollTop(height);
    }

    utteranceInput.addEventListener("keypress", (e) => {
        if (e.keyCode == 13) {
            userSpeak();
        }
    });

    button.addEventListener("click", () => {
        userSpeak();
    });

    function userSpeak() {
        if (button.botListens) {
            let utteranceString = utteranceInput.value;

            messagesList.append("<li class='message users'>" + utteranceString + "</li>");

            BotUtils.serverLog("User", utteranceString, theBot.sessionId);

            button.botListens(utteranceString);

            utteranceInput.value = "";
            utteranceInput.focus();

            scrollChat();
        }
    }

    let coveredTopicsList = $("#coveredTopicsList");
    let goals = $("#goal-list");
    let slots = $("#slot-list");
    let actionsQueue = $("#actions-list");

    window.document.addEventListener("chatterbot.speak", (e) => {
        // setTimeout(() => {
            messagesList.append("<li class='message bots'>" + e.detail.utterance + "</li>");
        // }, 1000 * _.random(1,3));
        messagesList.scrollTop(500);

        BotUtils.serverLog("Bot", e.detail.utterance, theBot.sessionId);

        scrollChat();
    });

    window.document.addEventListener("chatterbot.refresh", (e) => {
        {
            // show topics
            let list = coveredTopicsList.find("ul");
            list.html("");
            /*_.each(myBot._memory.covered_topics, (val, topicName) => {
                list.append("<li>" + topicName + "</li>");
            });*/

            /*if (_.size(myBot._memory.covered_topics) == 0) {
                list.append("<li>No topics yet!</li>");
            }*/

            let goalList = goals.find("ul");
            goalList.html("");
            _.each(theBot._allGoals, (goal) => {
                let activeStr = goal.active ? "active" : "inactive";
                let solvedStr = goal.solved ? "solved" : "pending";
                let elem = $(`<li>${goal.name} (${activeStr}) (${solvedStr})</li>`);
                if (goal.solved) {
                    elem.addClass("solved");
                }
                if (goal.active) {
                    elem.addClass("active");
                }
                goalList.append(elem);
            });

            let slotList = slots.find("ul");
            slotList.html("");
            _.each(theBot.slots, (slotArray, groupName) => {
                slotList.append(`<li>${groupName}</li>`);
                let subList = $("<ul></ul>");
                _.each(slotArray, (slot) => {
                    if (slot.instance) {
                        let slotString = slot.instance.toString();

                        subList.append(`<li>entity: ${slot.name} value: ${slotString}</li>`);
                    } else {
                        subList.append(`<li>entity: ${slot.name} value: ${slot.value || false}</li>`);
                    }
                    /*if (slot.type == "FlightEntity") {
                        let value = false;
                        if (slot.value) {
                            value = `Airline:${slot.value.airline} * Code: ${slot.value.code} * Cost: $${slot.value.cost}`;
                        }
                        subList.append(`<li>entity: ${slot.name} value: ${value}</li>`);
                    }*/
                });
                slotList.append(subList);
            });

            let actionList = actionsQueue.find("ul");
            actionList.html("");
            if (theBot._currentAction) {
                actionList.append(`<li><strong>Current: ${theBot._currentAction}</strong></li>`);
            }
            _.each(theBot._actionsQueue, (action) => {
                actionList.append(`<li>${action}</li>`);
            });
        }

    });

    theBot.execute({
        debug: true
    });
});