var moment = require("moment");
var SlotRestriction = require("../../../build-common/js/chatterbot/model/SlotRestriction").default;
var Slots = require("../../../build-common/js/chatterbot/model/Slots");
var SlotFactory = require("../../../build-common/js/chatterbot/model/SlotFactory").default;

var restrictions = [
    {
        "goal": "check-flight-status",
        "slot": "flight-code",
        "value": "AA-542",
        "type": "equality"
    },
    {
        "goal": "set-origin",
        "slot": "departure_location",
        "value": ["Buenos Aires", "Cordoba"],
        "type": "inset"
    },
    {
        "goal": "set-destination",
        "slot": "destination_location",
        "value": "Amsterdam",
        "type": "equality"
    },
    {
        // TODO: find a way to calculate the interval value
        "goal": "set-departure-datetime",
        "slot": "departure-date",
        "value": { "from": "2017-09-20T00:00-03:00", "to": "2017-09-27T00:00-03:00" },
        "rules": [
            "restrictions:morning"
        ],
        "type": "date-interval"
    },
    {
        "goal": "after-purchase",
        "slot": "has-luggage",
        "value": true,
        "type": "boolean"
    },
    {
        "goal": "print-boarding-pass",
        "slot": "username",
        "value": "Juan Villegas",
        "type": "equality"
    },
    {
        "goal": "print-boarding-pass",
        "slot": "id",
        "value": "34666036",
        "type": "equality"
    }
];

var restrictionsArray = [];
restrictions.forEach(r => {
    restrictionsArray.push(SlotRestriction.createFromDefinition(r));
});

var simpleSlot = SlotFactory.create("value", { value: "AA-542" });
var locationSlot = SlotFactory.create("location", { value: "Cordoba" });
var momentNow = moment();
var dateSlot = SlotFactory.create("date-interval", {
    value: "",
    from: momentNow.clone().add(0, "days"),
    to: momentNow.clone().add(5, "days")
});

console.log(restrictionsArray[0].isValid(simpleSlot));
console.log(restrictionsArray[1].isValid(locationSlot));
console.log(restrictionsArray[3].isValid(dateSlot));
