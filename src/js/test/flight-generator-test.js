var Generator = require("../../../build-common/js/scenarios/flight-reservation/components/flight-generator").default;
var moment = require("moment");

var now = moment();

var data = Generator.generateFlights({
	"departure_location": "Buenos Aires",
	"destination_location": "Florianopolis",
	"departure_datetime": { from: now.clone().add(2, "days"), to: now.clone().add(3, "days") }
});

console.log(data);