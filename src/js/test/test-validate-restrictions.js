var Tutor = require("../../../build-common/js/chatterbot/tutor/Tutor").default;
var Chatterbot = require("../../../build-common/js/chatterbot/chatterbot").default;
var scenarioDescriptor = require("../../../build-common/js/scenarios/flight-reservation/data/scenario-descriptor").default;
var moment = require("moment");
var SlotRestriction = require("../../../build-common/js/chatterbot/model/SlotRestriction").default;
var Slots = require("../../../build-common/js/chatterbot/model/Slots");
var SlotFactory = require("../../../build-common/js/chatterbot/model/SlotFactory").default;

var bot = new Chatterbot(null, {}, scenarioDescriptor);

var simpleSlot = SlotFactory.create("value", { value: "AA-542" });
var locationSlot = SlotFactory.create("location", { value: "Cordoba" });
var momentNow = moment();
var dateSlot = SlotFactory.create("date-interval", {
    value: "",
    from: momentNow.clone().add(0, "days"),
    to: momentNow.clone().add(5, "days")
});

bot.tutor.validateRestrictions(bot._scenarioRestrictions, bot._allGoals, bot.slots);